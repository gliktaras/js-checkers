debug : build_coffee build_tests build_sass

release : build_coffee build_sass build_optimized clean_optimized


build_coffee :
	coffee --compile --output checkers/js checkers/coffee

build_tests :
	coffee --compile --output checkers/tests/js checkers/tests/coffee

build_sass :
	sass --update checkers/sass:checkers/css

build_optimized :
	node r.js -o checkers/app.build.js

clean_optimized :
	find checkers-build/js -type f | grep -Ev "main.js|require.js" | xargs rm
	find checkers-build/js -depth -type d -empty -exec rmdir {} \;
	rm -rf checkers-build/app.build.js checkers-build/build.txt
