# js-checkers

A game of checkers, implemented using web technologies. This was my pet project
when I was learning web development.

There are no "graphics" or AI to speak of, but the logic of the game has been
fully implemented and it has appropriate unit test coverage. Various rule
variants are supported (such as board size and queen behaviour).


# Building

## Requirements

* CoffeeScript compiler (checked with version 1.3.3) ([link](http://coffeescript.org/))
* Node.js (checked with version 0.8.5) ([link](http://nodejs.org/))
* Ruby runtime (checked with version 1.9.3) ([link](http://www.ruby-lang.org/))
* Sass compiler (checked with version 3.1.19) ([link](http://sass-lang.com/))

## Procedure

    git clone https://gliktaras@bitbucket.org/gliktaras/js-checkers.git
    cd js-checkers
    make

The makefile provides two targets of interest. `debug` will build the project
without any optimizations, as well as build the test suite. `release` will
create an optimized build in `./checkers-build` directory of the project.
`debug` target is the default.

If you are on Windows, you have the option to use one of the provided batch
files if you do not wish to set up make.


# Licensing

This project uses BSD 2-Clause license.
