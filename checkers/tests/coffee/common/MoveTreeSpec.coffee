# MoveTreeSpec.coffee
# Unit tests for the MoveTree structure.

define(
    ["common/MoveTree"]
    (MoveTree) ->

        describe("MoveTree", ->

            it("can be correctly constructed without a kill parameter", ->
                arg1 = { row: 1, col: 2 }
                tree = new MoveTree(arg1, null)

                expect(tree.own).toEqual(arg1)
                expect(tree.kill).toEqual(null)
                expect(tree.height).toEqual(0)
                expect(tree.next).toEqual([])
            )
            it("can be correctly constructed with a kill parameter", ->
                arg1 = { row: 1, col: 2 }
                arg2 = { row: 3, col: 4 }
                tree = new MoveTree(arg1, arg2)

                expect(tree.own).toEqual(arg1)
                expect(tree.kill).toEqual(arg2)
                expect(tree.height).toEqual(0)
                expect(tree.next).toEqual([])
            )

            it("can expand", ->
                tree1 = new MoveTree({ row: 1, col: 1 }, null)
                tree2 = new MoveTree({ row: 2, col: 2 }, { row: 3, col: 3 })
                tree1.addTree(tree2)

                expect(tree1.height).toEqual(1)
                expect(tree1.next).toEqual([tree2])
            )

            it("can maintain its height when built bottom-up", ->
                tree1 = new MoveTree({ row: 1, col: 1 }, null)
                tree2 = new MoveTree({ row: 2, col: 2 }, { row: 3, col: 3 })
                tree3 = new MoveTree({ row: 4, col: 4 }, { row: 5, col: 5 })
                tree4 = new MoveTree({ row: 6, col: 6 }, { row: 7, col: 7 })

                tree3.addTree(tree4)
                tree1.addTree(tree2)
                tree1.addTree(tree3)

                expect(tree1.height).toEqual(2)
                expect(tree2.height).toEqual(0)
                expect(tree3.height).toEqual(1)
                expect(tree4.height).toEqual(0)
            )

            it("can identify leaf nodes", ->
                tree = new MoveTree({ row: 1, col: 1 }, null)

                expect(tree.isLeaf()).toEqual(true)
            )
            it("can identify non-leaf nodes", ->
                tree1 = new MoveTree({ row: 1, col: 1 }, null)
                tree2 = new MoveTree({ row: 2, col: 2 }, { row: 3, col: 3 })
                tree1.addTree(tree2)

                expect(tree1.isLeaf()).toEqual(false)
            )

            it("can relabel itself correctly", ->
                tree = new MoveTree({ row: 1, col: 3 })
                tree.addTree(new MoveTree({ row: 3, col: 1 }, { row: 2, col: 2 }))
                tree.next[0].addTree(new MoveTree({ row: 5, col: 3 }, { row: 4, col: 2 }))
                tree.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                tree.next[1].addTree(new MoveTree({ row: 5, col: 7 }, { row: 4, col: 6 }))
                tree.next[1].next[0].addTree(new MoveTree({ row: 7, col: 5 }, { row: 6, col: 6 }))
                tree.relabelHeights()

                expect(tree.height).toEqual(3)
                expect(tree.next[0].height).toEqual(1)
                expect(tree.next[0].next[0].height).toEqual(0)
                expect(tree.next[1].height).toEqual(2)
                expect(tree.next[1].next[0].height).toEqual(1)
                expect(tree.next[1].next[0].next[0].height).toEqual(0)
            )
        )
)
