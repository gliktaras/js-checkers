# utilsSpec.coffee
# Unit tests for utilities.

define(
    ["common/constants", "common/MoveTree", "common/utils"]
    (constants, MoveTree, utils) ->

        describe("Utilities", ->

            describe("column label calculation function", ->
                it("can compute column labels", ->
                    labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    for i in [1..200]
                        expect(utils.getColumnLabel(i)).toEqual(labels[ (i-1) % 26 ])
                )
            )

            describe("move tree search function", ->
                moveTreeList = []

                beforeEach( ->
                    moveTreeList = []
                    moveTreeList.push(new MoveTree({ row: 1, col: 1 }, null))
                    moveTreeList.push(new MoveTree({ row: 2, col: 2 }, { row: 3, col: 3 }))
                    moveTreeList[0].addTree(new MoveTree({ row: 5, col: 5 }, null))
                )
                afterEach( ->
                    moveTreeList = []
                )
    
                it("cannot match a MoveTree that does not exist", ->
                    expect(utils.getMoveTreeNode(moveTreeList, 0, 0)).toBeFalsy()
                )
                it("can match a MoveTree without kill variable set", ->
                    expect(utils.getMoveTreeNode(moveTreeList, 1, 1)).toEqual(moveTreeList[0])
                )
                it("can match a MoveTree with kill variable set", ->
                    expect(utils.getMoveTreeNode(moveTreeList, 2, 2)).toEqual(moveTreeList[1])
                )
                it("cannot match a MoveTree by its kill variable", ->
                    expect(utils.getMoveTreeNode(moveTreeList, 3, 3)).toBeFalsy()
                )
                it("cannot match a MoveTree on a lower level", ->
                    expect(utils.getMoveTreeNode(moveTreeList, 5, 5)).toBeFalsy()
                )
            )

            describe("opposite player location function", ->
                it("can find the opponent of the white player", ->
                    expect(utils.getOtherPlayer(constants.players.WHITE)).toEqual(constants.players.BLACK)
                )
                it("can find the opponent of the black player", ->
                    expect(utils.getOtherPlayer(constants.players.BLACK)).toEqual(constants.players.WHITE)
                )
                it("can correctly react when no player is given", ->
                    expect(utils.getOtherPlayer(constants.players.NONE)).toEqual(constants.players.NONE)
                )
                it("can correctly react when random trash is given", ->
                    expect(utils.getOtherPlayer(124)).toEqual(constants.players.NONE)
                )
            )

            describe("sign location function", ->
                it("can identify the sign of a positive integer", ->
                    expect(utils.getSign(5)).toEqual(1)
                )
                it("can identify the sign of a negative integer", ->
                    expect(utils.getSign(-4)).toEqual(-1)
                )
                it("can identify the sign of a zero", ->
                    expect(utils.getSign(0)).toEqual(0)
                )
            )

            describe("cell color identification function", ->
                it("can identify a bright cell", ->
                    expect(utils.isCellDark(0, 0)).toBeFalsy()
                )
                it("can identify another bright cell", ->
                    expect(utils.isCellDark(1, 3)).toBeFalsy()
                )
                it("can identify a dark cell", ->
                    expect(utils.isCellDark(2, 3)).toBeTruthy()
                )
                it("can identify another dark cell", ->
                    expect(utils.isCellDark(5, 6)).toBeTruthy()
                )
            )

        )
)
