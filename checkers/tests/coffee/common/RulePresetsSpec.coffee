# RulePresetsSpec.coffee
# Unit tests for rule preset list.

define(
    ["common/RulePresets"]
    (presets) ->

        describe("Rule presets", ->
            it("specify full ruleset name", ->
                for k, v of presets
                    expect(v.fullRulesetName).toBeDefined()
            )

            it("specify the board size", ->
                for k, v of presets
                    expect(v.boardSize).toBeDefined()
            )
            it("specify whether maximum attack distance should be forced", ->
                for k, v of presets
                    expect(v.forceMaxAttack).toBeDefined()
            )
            it("specify whether queens have free attacking range", ->
                for k, v of presets
                    expect(v.freeQueenRange).toBeDefined()
            )
            it("specify mid-attack piece promotion rule", ->
                for k, v of presets
                    expect(v.midAttackPromotion).toBeDefined()
            )
            it("specify normal piece attack direction rule", ->
                for k, v of presets
                    expect(v.normalAttackDir).toBeDefined()
            )
            it("specify the amount of checker-filled rows", ->
                for k, v of presets
                    expect(v.pieceRows).toBeDefined()
            )
        )
)

