# GameStatViewSpec.coffee
# Unit tests for the game statistic view.

define(
    ["jquery", "backbone", "common/constants", "views/GameStatView"]
    ($, Backbone, constants, GameStatView) ->

        describe("GameStatView", ->
            GameModel = Backbone.Model.extend(
                defaults:
                    curPlayer: constants.players.WHITE
                    turnNumber: 1
                    winner: constants.players.NONE

                getWinner: -> @get("winner")
            )

            model = null
            gameStatView = null


            beforeEach( ->
                setFixtures(sandbox( id: "view-container" ))

                model = new GameModel()
                gameStatView = new GameStatView( el: "#view-container", model: model )
            )

            it("can initialize", ->
                onSpy = spyOn(model, "on")
                updateSpy = spyOn(gameStatView, "_updateState")

                gameStatView.initialize()

                expect(onSpy).toHaveBeenCalledWith("change", gameStatView._updateState)
                expect(updateSpy).toHaveBeenCalled()
            )
            it("can set up the appropriate DOM", ->
                expect($("#view-container")).toContain("#move-counter")
                expect($("#view-container")).toContain("#turn-tracker")
                expect($("#view-container")).toContain("#game-message")
            )

            it("can set the move counter to correct value (2)", ->
                model.set( turnNumber: 2 )
                expect($("#move-counter").text()).toEqual("Move: 2")
            )
            it("can set the move counter to correct value (114)", ->
                model.set( turnNumber: 114 )
                expect($("#move-counter").text()).toEqual("Move: 114")
            )

            it("can set the turn tracker to white player", ->
                model.set( curPlayer: constants.players.WHITE )
                expect($("#turn-tracker").text()).toEqual("Turn: WHITE")
            )
            it("can set the turn tracker to black player", ->
                model.set( curPlayer: constants.players.BLACK )
                expect($("#turn-tracker").text()).toEqual("Turn: BLACK")
            )

            it("can hide the message if the game is in progress", ->
                model.set( winner: constants.players.NONE )

                expect($("#move-counter").hasClass("hidden")).toEqual(false)
                expect($("#turn-tracker").hasClass("hidden")).toEqual(false)
                expect($("#game-message").hasClass("hidden")).toEqual(true)
            )
            it("can show correct message if white player won", ->
                model.set( winner: constants.players.WHITE )

                expect($("#move-counter").hasClass("hidden")).toEqual(true)
                expect($("#turn-tracker").hasClass("hidden")).toEqual(true)
                expect($("#game-message").hasClass("hidden")).toEqual(false)
                expect($("#game-message").text()).toEqual("WHITE player wins")
            )
            it("can show correct message if black player won", ->
                model.set( winner: constants.players.BLACK )

                expect($("#move-counter").hasClass("hidden")).toEqual(true)
                expect($("#turn-tracker").hasClass("hidden")).toEqual(true)
                expect($("#game-message").hasClass("hidden")).toEqual(false)
                expect($("#game-message").text()).toEqual("BLACK player wins")
            )
        )
)
