# SettingsViewSpec.coffee
# Unit tests for the settings view.

define(
    ["backbone", "common/constants", "common/RulePresets", "views/SettingsView"]
    (Backbone, constants, presets, SettingsView) ->

        describe("SettingsView", ->
            settingsModel = null
            settingsView = null

            beforeEach( ->
                setFixtures(
                    """
                    <div id="settings">
                        <div class="rules">
                            <div class="line">
                                <label for="rule-presets">Preset</label>
                                <select id="rule-presets"></select>
                            </div>
                            <div class="spacer"></div>
                            <div class="line">
                                <label for="board-size-field">Board size</label>
                                <input type="text" id="board-size-field">
                            </div>
                            <div class="line">
                                <label for="piece-rows-field">Rows to fill</label>
                                <input type="text" id="piece-rows-field">
                            </div>
                            <div class="spacer"></div>
                            <div class="line">
                                <label for="attack-rules">Piece attack rules</label>
                            </div>
                            <div class="line">
                                <select id="attack-rules">
                                    <option value="FORWARD_ONLY">Forward only</option>
                                    <option value="FORWARD_START_ONLY">First attack forward</option>
                                    <option value="ANY_DIRECTION">Any direction</option>
                                </select>
                            </div>
                            <div class="line">
                                <label for="promotion-rules">Mid-attack promotion</label>
                            </div>
                            <div class="line">
                                <select id="promotion-rules">
                                    <option value="NO_PROMOTION">No promotion</option>
                                    <option value="PROMOTE_AND_STOP">Promote and stop attack</option>
                                    <option value="PROMOTE_AND_CONTINUE">Promote and continue move</option>
                                </select>
                            </div>
                            <div class="spacer"></div>
                            <div class="line">
                                <input type="checkbox" id="max-attacks-checkbox">
                                <label for="max-attacks-checkbox">Force maximum attacks</label>
                            </div>
                            <div class="line">
                                <input type="checkbox" id="queen-distance-checkbox">
                                <label for="queen-distance-checkbox">Queens move any distance</label>
                            </div>
                            <div class="spacer"></div>
                            <div class="line">
                                <div class="error-message"></div>
                            </div>
                            <div class="line">
                                <button type="button" id="apply-button">Apply rules</button>
                            </div>
                        </div>
                        <div id="options">
                            <div class="line">
                                <input type="checkbox" id="suicide-checkers-checkbox">
                                <label for="suicide-checkers-checkbox">Suicide checkers</label>
                            </div>
                            <div class="line">
                                <input type="checkbox" id="white-starts-checkbox">
                                <label for="white-starts-checkbox">White start</label>
                            </div>
                            <div class="line">
                                <input type="checkbox" id="white-on-top-checkbox">
                                <label for="white-on-top-checkbox">White on top</label>
                            </div>
                            <div class="spacer"></div>
                            <div class="line">
                                <button type="button" id="reset-button">Start new game</button>
                            </div>
                        </div>
                    </div>
                    """)

                settingsModel = new Backbone.Model()
                settingsModel.resetGameState = ->

                settingsView = new SettingsView(el: "#settings", model: settingsModel)
            )


            it("can initialise", ->
                makeDomSpy = spyOn(settingsView, "_createDOM")
                setRulesSpy = spyOn(settingsView, "_setRulesToCurrentPreset")

                settingsView.initialize()

                expect(makeDomSpy).toHaveBeenCalled()
                expect(setRulesSpy).toHaveBeenCalled()
            )

            it("can populate the preset select box", ->
                for name, rules in presets
                    expect($("#rule-presets option[value=#{name}]")).toExist()
                expect($("#rule-presets option[value=custom]")).toExist()
            )

            it("can start a new game when an appropriate button is clicked", ->
                resetSpy = spyOn(settingsModel, "resetGameState")
                $("#reset-button").click()
                expect(resetSpy).toHaveBeenCalled()
            )


            describe("updating rule UI elements to match current preset", ->
                curPreset = null

                beforeEach( ->
                    $("#rule-presets option[value=international]").prop("selected", true)
                    settingsView._setRulesToCurrentPreset()
                    curPreset = presets["international"]
                )

                it("can update the piece attack rule select box", ->
                    expected = curPreset.normalAttackDir
                    expect($("#attack-rules option[value=#{expected}]").prop("selected")).toEqual(true)
                )
                it("can update the mid-attack promotion rule select box", ->
                    expected = curPreset.midAttackPromotion
                    expect($("#promotion-rules option[value=#{expected}]").prop("selected")).toEqual(true)
                )
                it("can update the board size field", ->
                    expect($("#board-size-field").val()).toEqual(String(curPreset.boardSize))
                )
                it("can update the force max attacks checkbox", ->
                    expect($("#max-attacks-checkbox").prop("checked")).toEqual(curPreset.forceMaxAttack)
                )
                it("can update the free queen range checkbox", ->
                    expect($("#queen-distance-checkbox").prop("checked")).toEqual(curPreset.freeQueenRange)
                )
                it("can update the rows to fill field", ->
                    expect($("#piece-rows-field").val()).toEqual(String(curPreset.pieceRows))
                )
            )

            describe("changing rule UI elements sets preset to custom", ->
                beforeEach( ->
                    $("#rule-presets option[value=international]").prop("selected", true)
                    settingsView._setRulesToCurrentPreset()
                )
                afterEach( ->
                    expect($("#rule-presets option[value=custom]").prop("selected")).toEqual(true)
                )

                it("can correctly behave when piece attack direction rule is changed", ->
                    $("#attack-rules").change()
                )
                it("can correctly behave when mid-attack piece promotion rule is changed", ->
                    $("#promotion-rules").change()
                )
                it("can correctly behave when board size field is changed", ->
                    $("#board-size-field").change()
                )
                it("can correctly behave when force max attacks checkbox is changed", ->
                    $("#max-attacks-checkbox").change()
                )
                it("can correctly behave when free queen range checkbox is changed", ->
                    $("#queen-distance-checkbox").change()
                )
                it("can correctly behave when rows to fill field is changed", ->
                    $("#piece-rows-field").change()
                )
            )


            describe("changing option UI elements to current model values", ->
                beforeEach( ->
                    settingsModel.set(
                        suicide: false
                        whiteOnTop: false
                        whiteStarts: false
                    ,
                        silent: true
                    )
                    settingsView._setOptionsToCurrentModel()
                )

                it("can update the suicide checkers checkbox", ->
                    expect($("#suicide-checkers-checkbox").prop("checked")).toEqual(settingsModel.get('suicide'))
                )
                it("can update the white start checkbox", ->
                    expect($("#white-starts-checkbox").prop("checked")).toEqual(settingsModel.get('whiteStarts'))
                )
                it("can update the white on top checkbox", ->
                    expect($("#white-on-top-checkbox").prop("checked")).toEqual(settingsModel.get('whiteOnTop'))
                )
            )


            describe("updating model to the values in UI", ->
                beforeEach( ->
                    settingsModel.set(
                        boardSize: 1
                        forceMaxAttack: false
                        freeQueenRange: false
                        midAttackPromotion: ''
                        normalAttackDir: ''
                        pieceRows: 1
                        suicide: false
                        whiteOnTop: false
                        whiteStarts: false
                    ,
                        silent: true
                    )
                )

                it("can update the model when normal attack direction rule select box is changed", ->
                    $("#attack-rules option[value=ANY_DIRECTION]").prop("selected")
                    settingsView._onApplyRules()
                    expect(settingsModel.get("normalAttackDir")).toEqual(constants.normalAttackRules.ANY_DIRECTION)
                )
                it("can update the model when board size field is changed", ->
                    $("#promotion-rules option[value=PROMOTE_AND_STOP]").prop("selected")
                    settingsView._onApplyRules()
                    expect(settingsModel.get("midAttackPromotion")).toEqual(constants.midAttackPromotionRules.PROMOTE_AND_STOP)
                )
                it("can update the model when board size field is changed", ->
                    $("#board-size-field").val(10)
                    settingsView._onApplyRules()
                    expect(settingsModel.get("boardSize")).toEqual(10)
                )
                it("can update the model when force max attacks checkbox is changed", ->
                    $("#max-attacks-checkbox").prop("checked", true)
                    settingsView._onApplyRules()
                    expect(settingsModel.get("forceMaxAttack")).toEqual(true)
                )
                it("can update the model when free queen range checkbox is changed", ->
                    $("#queen-distance-checkbox").prop("checked", true)
                    settingsView._onApplyRules()
                    expect(settingsModel.get("freeQueenRange")).toEqual(true)
                )
                it("can update the model when rows to fill field is changed", ->
                    $("#piece-rows-field").val(10)
                    settingsView._onApplyRules()
                    expect(settingsModel.get("pieceRows")).toEqual(10)
                )
                it("can update the model when suicide checkers checkbox is changed", ->
                    $("#suicide-checkers-checkbox").prop("checked", true)
                    settingsView._onApplyRules()
                    expect(settingsModel.get("suicide")).toEqual(true)
                )
                it("can update the model when white start checkbox is changed", ->
                    $("#white-starts-checkbox").prop("checked", true)
                    settingsView._onApplyRules()
                    expect(settingsModel.get("whiteStarts")).toEqual(true)
                )
                it("can update the model when white on top checkbox is changed", ->
                    $("#white-on-top-checkbox").prop("checked", true)
                    settingsView._onApplyRules()
                    expect(settingsModel.get("whiteOnTop")).toEqual(true)
                )
            )


            describe("updating model to the values in UI", ->
                beforeEach( ->
                    settingsModel.set(
                        suicide: false
                        whiteOnTop: false
                        whiteStarts: false
                    ,
                        silent: true
                    )
                    $("#rule-presets option[value=international]").prop("selected", true)
                    settingsView._setOptionsToCurrentModel()
                )
                afterEach( ->
                    expect($("#rule-presets option[value=international]").prop("selected")).toEqual(true)
                )

                it("can update the model when suicide checkers checkbox is changed", ->
                    $("#suicide-checkers-checkbox").prop("checked", true)
                    settingsView._onNewGame()
                    expect(settingsModel.get("suicide")).toEqual(true)
                )
                it("can update the model when white start checkbox is changed", ->
                    $("#white-starts-checkbox").prop("checked", true)
                    settingsView._onNewGame()
                    expect(settingsModel.get("whiteStarts")).toEqual(true)
                )
                it("can update the model when white on top checkbox is changed", ->
                    $("#white-on-top-checkbox").prop("checked", true)
                    settingsView._onNewGame()
                    expect(settingsModel.get("whiteOnTop")).toEqual(true)
                )
            )


            describe("error handling logic", ->
                beforeEach( ->
                    settingsModel.set(
                        boardSize: 1
                        forceMaxAttack: false
                        freeQueenRange: false
                        midAttackPromotion: ''
                        normalAttackDir: ''
                        pieceRows: 1
                        suicide: false
                        whiteOnTop: false
                        whiteStarts: false
                    ,
                        silent: true
                    )
                )

                it("can mark board size field as invalid when a non-integer is entered", ->
                    $("#board-size-field").val("xx").change()
                    expect($("#board-size-field").closest("div").hasClass("invalid")).toBeTruthy()
                )
                it("can show a message if user tries to submit with erroneous board size", ->
                    $("#board-size-field").val("xx").change()
                    settingsView._onApplyRules()
                    expect($(".error-message").text()).not.toEqual("")
                )
                it("can prevent model modification if user tries to submit with erroneous board size", ->
                    $("#board-size-field").val("xx").change()
                    settingsView._onApplyRules()
                    expect(settingsModel.get("boardSize")).toEqual(1)
                )

                it("can mark piece rows field as invalid when a non-integer is entered", ->
                    $("#board-size-field").val("xx").change()
                    expect($("#board-size-field").closest("div").hasClass("invalid")).toBeTruthy()
                )
                it("can show a message if user tries to submit with erroneous piece rows value", ->
                    $("#piece-rows-field").val("xx").change()
                    settingsView._onApplyRules()
                    expect($(".error-message").text()).not.toEqual("")
                )
                it("can prevent model modification if user tries to submit with erroneous piece rows value", ->
                    $("#piece-rows-field").val("xx").change()
                    settingsView._onApplyRules()
                    expect(settingsModel.get("boardSize")).toEqual(1)
                )

                it("can clear the invalid fields", ->
                    $("#board-size-field").val("xx").change()
                    $("#piece-rows-field").val("xx").change()
                    $("#rule-presets option[value=international]").prop("selected", true).change()
                    expect($(".invalid").length).toEqual(0)
                )
                it("can clear the error message", ->
                    $(".error-message").text("You are invalid.")
                    settingsView._setRulesToCurrentPreset()
                    expect($(".error-message").text()).toEqual("")
                )
            )
        )
)
