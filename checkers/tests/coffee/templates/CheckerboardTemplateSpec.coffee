# CheckerboardTemplateSpec.coffee
# Unit tests for checkboard template generator.

define(
    ["jquery", "templates/CheckerboardTemplate"]
    ($, templateGen) ->

        describe("CheckerboardGenerator", ->
            template = null
            rows = null

            beforeEach( ->
                template = $(templateGen(4))
                rows = template.children('tbody').children('tr')
            )

            describe("DOM tree", ->
                it("can generate correct DOM tree", ->
                    expect(template.is("table")).toEqual(true)

                    body = template.children("tbody")
                    expect(body.length).toEqual(1)

                    rows = body.children("tr")
                    expect(rows.length).toEqual(5)

                    for i in [0..4]
                        cells = $(rows[i]).children("td")
                        expect(cells.length).toEqual(5)
                )
            )

            describe("column labels", ->
                it("can generate the column labels", ->
                    labels = "ABCD"
                    for i in [1..4]
                        el = $(rows[4]).children("td")[i]
                        expect(el.innerHTML).toEqual(labels[i-1])
                )
                it("can set classes of the column labels", ->
                    for i in [0..3]
                        el = $(rows[i]).children("td")[0]
                        expect($(el).hasClass("board-label board-row-label")).toEqual(true)
                )
            )

            describe("row labels", ->
                it("can generate the row labels", ->
                    for i in [1..4]
                        el = $(rows[4-i]).children("td")[0]
                        expect(el.innerHTML).toEqual("#{i}")
                )
                it("can set classes on the label elements", ->
                    for i in [1..4]
                        el = $(rows[4]).children("td")[i]
                        expect($(el).hasClass("board-label board-col-label")).toEqual(true)
                )
            )

            describe("board corner", ->
                it("can create the board corner", ->
                    expect($($(rows[4]).children("td")[0]).hasClass("board-corner")).toEqual(true)
                )
            )

            describe("board cells", ->
                it("can set the color of the cells", ->
                    for i in [1..4]
                        for j in [1..4]
                            if (i + j) % 2 == 0
                                el = $(rows[4-i]).children("td")[j]
                                expect($(el).hasClass("cell-bright")).toEqual((i + j) % 2 is 0)
                                expect($(el).hasClass("cell-dark")).toEqual((i + j) % 2 is 1)
                )
                it("can set ids on the cell elements", ->
                    for i in [1..4]
                        for j in [1..4]
                            el = $(rows[4-i]).children("td")[j]
                            expect($(el).attr("id")).toEqual("cell-#{i}-#{j}")
                )
            )

            describe("board edges", ->
                it("can set the top edge of the board", ->
                    for i in [1..4]
                        for j in [1..4]
                            el = $(rows[4-i]).children("td")[j]
                            expect($(el).hasClass("edge-top")).toEqual(i is 4)
                )
                it("can set the bottom edge of the board", ->
                    for i in [1..4]
                        for j in [1..4]
                            el = $(rows[4-i]).children("td")[j]
                            expect($(el).hasClass("edge-bottom")).toEqual(i is 1)
                )
                it("can set the left edge of the board", ->
                    for i in [1..4]
                        for j in [1..4]
                            el = $(rows[4-i]).children("td")[j]
                            expect($(el).hasClass("edge-left")).toEqual(j is 1)
                )
                it("can set the top edge of the board", ->
                    for i in [1..4]
                        for j in [1..4]
                            el = $(rows[4-i]).children("td")[j]
                            expect($(el).hasClass("edge-right")).toEqual(j is 4)
                )
            )

        )
)
