# GameModelSpec.coffee
# Unit tests for the model of the checkers game.

define(
    ["underscore", "common/constants", "common/MoveTree", "models/GameModel"]
    (_, constants, MoveTree, GameModel) ->

        describe("GameModel", ->
            model = null

            beforeEach( ->
                model = new GameModel()
            )


            it("can initialize", ->
                spy = spyOn(model, 'resetGameState')
                model.initialize()
                expect(spy).toHaveBeenCalled()
            )


            describe("game state reset", ->
                moveRefreshSpy = null

                beforeEach( ->
                    moveRefreshSpy = spyOn(model, '_computeAvailableMoves')
                )
                afterEach( ->
                    expect(moveRefreshSpy).toHaveBeenCalled()
                )

                it("can create the board of correct size (5)", ->
                    model.set( boardSize: 5 )

                    model.resetGameState()
                    expect(model.get("board").length).toEqual(5)
                    for i in [0..4]
                        expect(model.get("board")[i].length).toEqual(5)
                )
                it("can create the board of correct size (std)", ->
                    model.set( boardSize: 8 )

                    model.resetGameState()
                    expect(model.get("board").length).toEqual(8)
                    for i in [0..7]
                        expect(model.get("board")[i].length).toEqual(8)
                )

                it("can populate the board with checkers (5-1-W)", ->
                    expectedBoard = [
                        [ "_", "w", "_", "w", "_" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "_", "b", "_", "b", "_" ]
                    ].reverse()
                    model.set( boardSize: 5, pieceRows: 1, whiteOnTop: true )
                    model.resetGameState()

                    expect(model.get("board")).toEqual(expectedBoard)
                )
                it("can populate the board with checkers (5-1-B)", ->
                    expectedBoard = [
                        [ "_", "b", "_", "b", "_" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "_", "w", "_", "w", "_" ]
                    ].reverse()
                    model.set( boardSize: 5, pieceRows: 1, whiteOnTop: false )
                    model.resetGameState()

                    expect(model.get("board")).toEqual(expectedBoard)
                )
                it("can populate the board with checkers (5-2-W)", ->
                    expectedBoard = [
                        [ "_", "w", "_", "w", "_" ],
                        [ "w", "_", "w", "_", "w" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "b", "_", "b", "_", "b" ],
                        [ "_", "b", "_", "b", "_" ]
                    ].reverse()
                    model.set( boardSize: 5, pieceRows: 2, whiteOnTop: true )
                    model.resetGameState()

                    expect(model.get("board")).toEqual(expectedBoard)
                )
                it("can populate the board with checkers (5-2-B)", ->
                    expectedBoard = [
                        [ "_", "b", "_", "b", "_" ],
                        [ "b", "_", "b", "_", "b" ],
                        [ "_", "_", "_", "_", "_" ],
                        [ "w", "_", "w", "_", "w" ],
                        [ "_", "w", "_", "w", "_" ]
                    ].reverse()
                    model.set( boardSize: 5, pieceRows: 2, whiteOnTop: false )
                    model.resetGameState()

                    expect(model.get("board")).toEqual(expectedBoard)
                )
                it("can populate the board with checkers (std)", ->
                    expectedBoard = [
                        [ "w", "_", "w", "_", "w", "_", "w", "_" ],
                        [ "_", "w", "_", "w", "_", "w", "_", "w" ],
                        [ "w", "_", "w", "_", "w", "_", "w", "_" ],
                        [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                        [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                        [ "_", "b", "_", "b", "_", "b", "_", "b" ],
                        [ "b", "_", "b", "_", "b", "_", "b", "_" ],
                        [ "_", "b", "_", "b", "_", "b", "_", "b" ]
                    ].reverse()
                    model.set( boardSize: 8, pieceRows: 3, whiteOnTop: true )
                    model.resetGameState()

                    expect(model.get("board")).toEqual(expectedBoard)
                )

                it("can set correct piece counts (5-1)", ->
                    model.set( boardSize: 5, pieceRows: 1 )
                    model.resetGameState()

                    expect(model.get("blackCount")).toEqual(2)
                    expect(model.get("whiteCount")).toEqual(2)
                )
                it("can set correct piece counts (5-2)", ->
                    model.set( boardSize: 5, pieceRows: 2 )
                    model.resetGameState()

                    expect(model.get("blackCount")).toEqual(5)
                    expect(model.get("whiteCount")).toEqual(5)
                )
                it("can set correct piece counts (std)", ->
                    model.set( boardSize: 8, pieceRows: 3, whiteOnTop: true )
                    model.resetGameState()

                    expect(model.get("blackCount")).toEqual(12)
                    expect(model.get("whiteCount")).toEqual(12)
                )

                it("can set the white player to go first", ->
                    model.set( whiteStarts: true )
                    model.resetGameState()
                    expect(model.get("curPlayer")).toEqual(constants.players.WHITE)
                )
                it("can set the black player to go first", ->
                    model.set( whiteStarts: false )
                    model.resetGameState()
                    expect(model.get("curPlayer")).toEqual(constants.players.BLACK)
                )

                it("can reset the turn counter", ->
                    model.resetGameState()
                    expect(model.get("turnNumber")).toEqual(1)
                )
            )

            describe("non-attacking move calculations", ->
                describe("empty test", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_"],
                                [ "_", "_", "_", "_"],
                                [ "_", "_", "_", "_"],
                                [ "_", "_", "_", "_"]
                            ].reverse()
                            boardSize: 4
                        )
                    )

                    it("can return the correct result on empty board for the white player", ->
                        result = model._getNormalMoves(constants.players.WHITE)
                        expect(result).toEqual([])
                    )
                    it("can return the correct result on empty board for the black player", ->
                        result = model._getNormalMoves(constants.players.BLACK)
                        expect(result).toEqual([])
                    )
                )

                describe("unrestricted man moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "w", "_" ],
                                [ "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_" ]
                            ].reverse()
                            boardSize: 4
                            whiteOnTop: true
                        )
                    )

                    it("can find correct moves for upward-facing player", ->
                        expectedMoves = new MoveTree({ row: 1, col: 2 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 1 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 3 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.BLACK, 1, 2)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can find correct moves for downward-facing player", ->
                        expectedMoves = new MoveTree({ row: 4, col: 3 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 3, col: 2 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 3, col: 4 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 4, 3)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("wall restricted man moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "w", "_", "_", "w" ],
                                [ "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_" ],
                                [ "b", "_", "_", "b" ]
                            ].reverse()
                            boardSize: 4
                            whiteOnTop: true
                        )
                    )

                    it("can find correct moves for upward-facing player (wall left)", ->
                        expectedMoves = new MoveTree({ row: 1, col: 1 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 2 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.BLACK, 1, 1)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can find correct moves for upward-facing player (wall right)", ->
                        expectedMoves = new MoveTree({ row: 1, col: 4 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 3 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.BLACK, 1, 4)
                        expect(result).toEqual(expectedMoves)
                    )

                    it("can find correct moves for downward-facing player (wall left)", ->
                        expectedMoves = new MoveTree({ row: 4, col: 1 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 3, col: 2 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 4, 1)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can find correct moves for downward-facing player (wall right)", ->
                        expectedMoves = new MoveTree({ row: 4, col: 4 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 3, col: 3 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 4, 4)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("piece restricted men moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "w", "_", "w", "_", "_", "w", "_" ],
                                [ "_", "_", "b", "_", "_", "b", "_", "b" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "w", "_", "_", "w", "_", "w" ],
                                [ "_", "b", "_", "b", "_", "_", "b", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can find correct moves for upward-facing player (piece right)", ->
                        expectedMoves = new MoveTree({ row: 1, col: 2 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 1 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.BLACK, 1, 2)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can find correct moves for upward-facing player (piece left)", ->
                        expectedMoves = new MoveTree({ row: 1, col: 4 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 5 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.BLACK, 1, 4)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can find correct moves for upward-facing player (pieces both)", ->
                        expectedMoves = new MoveTree({ row: 1, col: 7 }, null)

                        result = model._getNormalMovesOfPiece(constants.players.BLACK, 1, 7)
                        expect(result).toEqual(expectedMoves)
                    )

                    it("can find correct moves for downward-facing player (piece right)", ->
                        expectedMoves = new MoveTree({ row: 8, col: 2 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 7, col: 1 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 8, 2)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can find correct moves for downward-facing player (piece left)", ->
                        expectedMoves = new MoveTree({ row: 8, col: 4 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 7, col: 5 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 8, 4)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can find correct moves for downward-facing player (pieces both)", ->
                        expectedMoves = new MoveTree({ row: 8, col: 7 }, null)

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 8, 7)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("wall restricted queen moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_" ],
                                [ "_", "W", "_", "_" ],
                                [ "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 4
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can find correct moves", ->
                        expectedMoves = new MoveTree({ row: 3, col: 2 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 1 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 2, col: 3 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 1, col: 4 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 1 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 3 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 3, 2)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("piece restricted queen moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "_", "_", "W", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "W", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "_", "_", "B", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 9
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can find correct moves", ->
                        expectedMoves = new MoveTree({ row: 5, col: 5 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 4 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 3, col: 3 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 6 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 3, col: 7 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 6, col: 4 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 7, col: 3 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 6, col: 6 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 7, col: 7 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 5, 5)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("queen move range restrictions", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "_", "_", "W", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "W", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "_", "_", "B", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 9
                            freeQueenRange: false
                            whiteOnTop: true
                        )
                    )

                    it("can find correct moves", ->
                        expectedMoves = new MoveTree({ row: 5, col: 5 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 4 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 6 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 6, col: 4 }, null))
                        expectedMoves.addTree(new MoveTree({ row: 6, col: 6 }, null))

                        result = model._getNormalMovesOfPiece(constants.players.WHITE, 5, 5)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("normal move reporting (men)", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "w", "_", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "b", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "b", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can correctly report moves for upward-facing player", ->
                        expectedMoves = []
                        expectedMoves.push(new MoveTree({ row: 1, col: 2 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 2, col: 1 }, null))
                        expectedMoves.push(new MoveTree({ row: 1, col: 4 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 2, col: 5 }, null))
                        expectedMoves.push(new MoveTree({ row: 2, col: 3 }, null))
                        expectedMoves[2].addTree(new MoveTree({ row: 3, col: 2 }, null))
                        expectedMoves[2].addTree(new MoveTree({ row: 3, col: 4 }, null))
                        expectedMoves.push(new MoveTree({ row: 3, col: 6 }, null))
                        expectedMoves[3].addTree(new MoveTree({ row: 4, col: 7 }, null))
                        tree.relabelHeights() for tree in expectedMoves

                        result = model._getNormalMoves(constants.players.BLACK)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can correctly report moves for downward-facing player", ->
                        expectedMoves = []
                        expectedMoves.push(new MoveTree({ row: 4, col: 5 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 3, col: 4 }, null))
                        expectedMoves.push(new MoveTree({ row: 7, col: 6 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 6, col: 5 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 6, col: 7 }, null))
                        expectedMoves.push(new MoveTree({ row: 8, col: 5 }, null))
                        expectedMoves[2].addTree(new MoveTree({ row: 7, col: 4 }, null))
                        expectedMoves.push(new MoveTree({ row: 8, col: 7 }, null))
                        expectedMoves[3].addTree(new MoveTree({ row: 7, col: 8 }, null))
                        tree.relabelHeights() for tree in expectedMoves

                        result = model._getNormalMoves(constants.players.WHITE)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("normal move reporting (queens)", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "W", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "W", "_", "_", "_" ],
                                [ "_", "_", "_", "B", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "B", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can correctly report moves for upward-facing player", ->
                        expectedMoves = []
                        expectedMoves.push(new MoveTree({ row: 2, col: 2 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 1, col: 1 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 1, col: 3 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 3, col: 1 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 3, col: 3 }, null))
                        expectedMoves.push(new MoveTree({ row: 4, col: 4 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 3, col: 3 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 3, col: 5 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 2, col: 6 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 1, col: 7 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 5, col: 3 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 6, col: 2 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 7, col: 1 }, null))
                        tree.relabelHeights() for tree in expectedMoves

                        result = model._getNormalMoves(constants.players.BLACK)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can correctly report moves for downward-facing player", ->
                        expectedMoves = []
                        expectedMoves.push(new MoveTree({ row: 5, col: 5 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 4, col: 6 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 3, col: 7 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 2, col: 8 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 6, col: 4 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 7, col: 3 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 8, col: 2 }, null))
                        expectedMoves[0].addTree(new MoveTree({ row: 6, col: 6 }, null))
                        expectedMoves.push(new MoveTree({ row: 7, col: 7 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 6, col: 6 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 6, col: 8 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 8, col: 6 }, null))
                        expectedMoves[1].addTree(new MoveTree({ row: 8, col: 8 }, null))
                        tree.relabelHeights() for tree in expectedMoves

                        result = model._getNormalMoves(constants.players.WHITE)
                        expect(result).toEqual(expectedMoves)
                    )
                )
            )


            describe("attacking move calculations", ->
                describe("empty test", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_"],
                                [ "_", "_", "_", "_"],
                                [ "_", "_", "_", "_"],
                                [ "_", "_", "_", "_"]
                            ].reverse()
                            boardSize: 4
                        )
                    )

                    it("can return the correct result on empty board for the white player", ->
                        result = model._getAttackMoves(constants.players.WHITE)
                        expect(result).toEqual([])
                    )
                    it("can return the correct result on empty board for the black player", ->
                        result = model._getAttackMoves(constants.players.BLACK)
                        expect(result).toEqual([])
                    )
                )

                describe("man attack start and their restrictions", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "B", "_", "b", "_", "_" ],
                                [ "_", "_", "_", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "B", "_", "b", "_", "_" ],
                                [ "_", "W", "_", "w", "_", "_", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "_", "_" ],
                                [ "_", "W", "_", "w", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can attack for an upward-facing player (forward only)", ->
                        expectedMoves = new MoveTree({ row: 3, col: 3 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 5, col: 1 }, { row: 4, col: 2 }))
                        expectedMoves.addTree(new MoveTree({ row: 5, col: 5 }, { row: 4, col: 4 }))

                        model.set("normalAttackDir", constants.normalAttackRules.FORWARD_ONLY)
                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 3, 3, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can attack for an upward-facing player (forward start only)", ->
                        expectedMoves = new MoveTree({ row: 3, col: 3 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 5, col: 1 }, { row: 4, col: 2 }))
                        expectedMoves.addTree(new MoveTree({ row: 5, col: 5 }, { row: 4, col: 4 }))

                        model.set("normalAttackDir", constants.normalAttackRules.FORWARD_START_ONLY)
                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 3, 3, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can attack for an upward-facing player (any direction)", ->
                        expectedMoves = new MoveTree({ row: 3, col: 3 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 5, col: 1 }, { row: 4, col: 2 }))
                        expectedMoves.addTree(new MoveTree({ row: 5, col: 5 }, { row: 4, col: 4 }))
                        expectedMoves.addTree(new MoveTree({ row: 1, col: 1 }, { row: 2, col: 2 }))
                        expectedMoves.addTree(new MoveTree({ row: 1, col: 5 }, { row: 2, col: 4 }))

                        model.set("normalAttackDir", constants.normalAttackRules.ANY_DIRECTION)
                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 3, 3, null)
                        expect(result).toEqual(expectedMoves)
                    )

                    it("can attack for an downward-facing player (forward only)", ->
                        expectedMoves = new MoveTree({ row: 6, col: 5 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 3 }, { row: 5, col: 4 }))
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 7 }, { row: 5, col: 6 }))

                        model.set("normalAttackDir", constants.normalAttackRules.FORWARD_ONLY)
                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 6, 5, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can attack for an downward-facing player (forward start only)", ->
                        expectedMoves = new MoveTree({ row: 6, col: 5 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 3 }, { row: 5, col: 4 }))
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 7 }, { row: 5, col: 6 }))

                        model.set("normalAttackDir", constants.normalAttackRules.FORWARD_START_ONLY)
                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 6, 5, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can attack for an downward-facing player (any direction)", ->
                        expectedMoves = new MoveTree({ row: 6, col: 5 }, null)
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 3 }, { row: 5, col: 4 }))
                        expectedMoves.addTree(new MoveTree({ row: 4, col: 7 }, { row: 5, col: 6 }))
                        expectedMoves.addTree(new MoveTree({ row: 8, col: 3 }, { row: 7, col: 4 }))
                        expectedMoves.addTree(new MoveTree({ row: 8, col: 7 }, { row: 7, col: 6 }))

                        model.set("normalAttackDir", constants.normalAttackRules.ANY_DIRECTION)
                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 6, 5, null)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("man attack blocks by walls", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "b", "_", "_", "_", "_", "_", "_", "b" ],
                                [ "_", "w", "_", "_", "_", "_", "w", "_" ],
                                [ "b", "_", "_", "_", "_", "_", "_", "b" ],
                                [ "w", "_", "_", "_", "_", "_", "_", "w" ],
                                [ "_", "b", "_", "_", "_", "_", "b", "_" ],
                                [ "w", "_", "_", "_", "_", "_", "_", "w" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
                            whiteOnTop: true
                        )
                    )

                    it("can exclude wall-blocked attacks for upward-facing player (wall left)", ->
                        expectedMoves = new MoveTree({ row: 3, col: 2 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 3, 2, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can exclude wall-blocked attacks for upward-facing player (wall right)", ->
                        expectedMoves = new MoveTree({ row: 3, col: 7 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 3, 7, null)
                        expect(result).toEqual(expectedMoves)
                    )

                    it("can exclude wall-blocked attacks for downward-facing player (wall left)", ->
                        expectedMoves = new MoveTree({ row: 6, col: 2 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 6, 2, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can exclude wall-blocked attacks for downward-facing player (wall right)", ->
                        expectedMoves = new MoveTree({ row: 6, col: 7 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 6, 7, null)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("man attack blocks by enemy pieces", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "b", "_", "_", "w", "b", "_" ],
                                [ "_", "_", "w", "b", "w", "b", "_", "_" ],
                                [ "_", "_", "_", "b", "w", "_", "_", "_" ],
                                [ "_", "_", "w", "b", "w", "b", "_", "_" ],
                                [ "_", "w", "b", "_", "_", "w", "b", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
                            whiteOnTop: true
                        )
                    )

                    it("can exclude enemy piece blocked attacks for upward-facing player", ->
                        expectedMoves = new MoveTree({ row: 4, col: 4 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 4, 4, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can exclude enemy piece blocked attacks for downward-facing player", ->
                        expectedMoves = new MoveTree({ row: 4, col: 5 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 4, 5, null)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("men attacking their own pieces", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "b", "w", "b", "w", "_" ],
                                [ "_", "_", "_", "b", "w", "_", "_" ],
                                [ "_", "_", "b", "w", "b", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
                            whiteOnTop: true
                        )
                    )

                    it("upward-facing player cannot attack own pieces", ->
                        expectedMoves = new MoveTree({ row: 4, col: 4 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 4, 4, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("downward-facing player cannot attack own pieces", ->
                        expectedMoves = new MoveTree({ row: 4, col: 5 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 4, 5, null)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("man attack blocks by own pieces behind enemies", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "w", "_", "_", "b", "w", "_" ],
                                [ "_", "_", "w", "b", "w", "b", "_", "_" ],
                                [ "_", "_", "_", "b", "w", "_", "_", "_" ],
                                [ "_", "_", "w", "b", "w", "b", "_", "_" ],
                                [ "_", "b", "w", "_", "_", "b", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
                            whiteOnTop: true
                        )
                    )

                    it("can exclude own piece blocked attacks for upward-facing player", ->
                        expectedMoves = new MoveTree({ row: 4, col: 4 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 4, 4, null)
                        expect(result).toEqual(expectedMoves)
                    )
                    it("can exclude own piece blocked attacks for downward-facing player", ->
                        expectedMoves = new MoveTree({ row: 4, col: 5 }, null)

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 4, 5, null)
                        expect(result).toEqual(expectedMoves)
                    )
                )

                describe("man attack chaining and branching", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "b", "_", "b", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "b", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "w", "_", "_", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            forceMaxAttack: false
                            whiteOnTop: true
                        )
                    )

                    it("can chain attacks for an upward-facing player", ->
                        moves = new MoveTree({ row: 1, col: 3 })
                        moves.addTree(new MoveTree({ row: 3, col: 1 }, { row: 2, col: 2 }))
                        moves.next[0].addTree(new MoveTree({ row: 5, col: 3 }, { row: 4, col: 2 }))
                        moves.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                        moves.next[1].addTree(new MoveTree({ row: 5, col: 7 }, { row: 4, col: 6 }))
                        moves.next[1].next[0].addTree(new MoveTree({ row: 7, col: 5 }, { row: 6, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 1, 3, null)
                        expect(result).toEqual(moves)
                    )
                    it("can chain attacks for a downward-facing player", ->
                        moves = new MoveTree({ row: 8, col: 5 })
                        moves.addTree(new MoveTree({ row: 6, col: 3 }, { row: 7, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 4, col: 1 }, { row: 5, col: 2 }))
                        moves.next[0].next[0].addTree(new MoveTree({ row: 2, col: 3 }, { row: 3, col: 2 }))
                        moves.addTree(new MoveTree({ row: 6, col: 7 }, { row: 7, col: 6 }))
                        moves.next[1].addTree(new MoveTree({ row: 4, col: 5 }, { row: 5, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 8, 5, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("maximum attack length rules", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "b", "_", "b", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "w", "_", "_", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can chain attacks for an upward-facing player (any distance)", ->
                        model.set(forceMaxAttack: false)

                        moves = new MoveTree({ row: 1, col: 3 })
                        moves.addTree(new MoveTree({ row: 3, col: 1 }, { row: 2, col: 2 }))
                        moves.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                        moves.next[1].addTree(new MoveTree({ row: 5, col: 7 }, { row: 4, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 1, 3, null)
                        expect(result).toEqual(moves)
                    )
                    it("can chain attacks for an upward-facing player (max distance)", ->
                        model.set(forceMaxAttack: true)

                        moves = new MoveTree({ row: 1, col: 3 })
                        moves.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 5, col: 7 }, { row: 4, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 1, 3, null)
                        expect(result).toEqual(moves)
                    )

                    it("can chain attacks for a downward-facing player (any distance)", ->
                        model.set(forceMaxAttack: false)

                        moves = new MoveTree({ row: 8, col: 5 })
                        moves.addTree(new MoveTree({ row: 6, col: 3 }, { row: 7, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 4, col: 1 }, { row: 5, col: 2 }))
                        moves.addTree(new MoveTree({ row: 6, col: 7 }, { row: 7, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 8, 5, null)
                        expect(result).toEqual(moves)
                    )
                    it("can chain attacks for a downward-facing player (max distance)", ->
                        model.set(forceMaxAttack: true)

                        moves = new MoveTree({ row: 8, col: 5 })
                        moves.addTree(new MoveTree({ row: 6, col: 3 }, { row: 7, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 4, col: 1 }, { row: 5, col: 2 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 8, 5, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("man mid-attack direction restrictions", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "w", "_", "_", "_" ],
                                [ "_", "b", "_", "b", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "w", "_", "w", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can find correct moves for an upward-facing player (forward only)", ->
                        model.set(normalAttackDir: constants.normalAttackRules.FORWARD_ONLY)

                        moves = new MoveTree({ row: 1, col: 3 })
                        moves.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 5, col: 7 }, { row: 4, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 1, 3, null)
                        expect(result).toEqual(moves)
                    )
                    it("can find correct moves for an upward-facing player (forward start only)", ->
                        model.set(normalAttackDir: constants.normalAttackRules.FORWARD_START_ONLY)

                        moves = new MoveTree({ row: 1, col: 3 })
                        moves.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 5, col: 7 }, { row: 4, col: 6 }))
                        moves.next[0].addTree(new MoveTree({ row: 1, col: 7 }, { row: 2, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 1, 3, null)
                        expect(result).toEqual(moves)
                    )
                    it("can find correct moves for an upward-facing player (any direction)", ->
                        model.set(normalAttackDir: constants.normalAttackRules.ANY_DIRECTION)

                        moves = new MoveTree({ row: 1, col: 3 })
                        moves.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 5, col: 7 }, { row: 4, col: 6 }))
                        moves.next[0].addTree(new MoveTree({ row: 1, col: 7 }, { row: 2, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 1, 3, null)
                        expect(result).toEqual(moves)
                    )

                    it("can chain attacks for a downward-facing player (forward only)", ->
                        model.set(normalAttackDir: constants.normalAttackRules.FORWARD_ONLY)

                        moves = new MoveTree({ row: 8, col: 5 })
                        moves.addTree(new MoveTree({ row: 6, col: 3 }, { row: 7, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 4, col: 1 }, { row: 5, col: 2 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 8, 5, null)
                        expect(result).toEqual(moves)
                    )
                    it("can chain attacks for a downward-facing player (forward start only)", ->
                        model.set(normalAttackDir: constants.normalAttackRules.FORWARD_START_ONLY)

                        moves = new MoveTree({ row: 8, col: 5 })
                        moves.addTree(new MoveTree({ row: 6, col: 3 }, { row: 7, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 4, col: 1 }, { row: 5, col: 2 }))
                        moves.next[0].addTree(new MoveTree({ row: 8, col: 1 }, { row: 7, col: 2 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 8, 5, null)
                        expect(result).toEqual(moves)
                    )
                    it("can chain attacks for a downward-facing player (any direction)", ->
                        model.set(normalAttackDir: constants.normalAttackRules.ANY_DIRECTION)

                        moves = new MoveTree({ row: 8, col: 5 })
                        moves.addTree(new MoveTree({ row: 6, col: 3 }, { row: 7, col: 4 }))
                        moves.next[0].addTree(new MoveTree({ row: 4, col: 1 }, { row: 5, col: 2 }))
                        moves.next[0].addTree(new MoveTree({ row: 8, col: 1 }, { row: 7, col: 2 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.WHITE,
                                constants.classes.CHECKER, 8, 5, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("man mid-attack direction restrictions", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "w", "_", "_", "_" ],
                                [ "_", "b", "_", "b", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "w", "_", "w", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )
                )

                describe("queen attack start", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "B", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "B", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can start an attack in forward direction", ->
                        moves = new MoveTree({ row: 3, col: 5 })
                        moves.addTree(new MoveTree({ row: 7, col: 1 }, { row: 6, col: 2 }))
                        moves.addTree(new MoveTree({ row: 6, col: 8 }, { row: 5, col: 7 }))

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 3, 5, null)
                        expect(result).toEqual(moves)
                    )
                    it("can start an attack in backward direction", ->
                        moves = new MoveTree({ row: 8, col: 4 })
                        moves.addTree(new MoveTree({ row: 5, col: 1 }, { row: 6, col: 2 }))
                        moves.addTree(new MoveTree({ row: 4, col: 8 }, { row: 5, col: 7 }))

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 8, 4, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("queen attack blocks by walls", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "w", "_", "_", "_", "_", "_", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "B", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "w", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "w" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can exclude wall-blocked attacks", ->
                        moves = new MoveTree({ row: 5, col: 4 })

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 5, 4, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("queen attack blocks by enemy pieces", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "w", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "B", "_", "_", "_", "_" ],
                                [ "_", "_", "w", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can exclude wall-blocked attacks", ->
                        moves = new MoveTree({ row: 5, col: 4 })

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 5, 4, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("queen attack blocks by own pieces", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "b", "_", "_" ],
                                [ "_", "_", "w", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "B", "_", "_", "_", "_" ],
                                [ "_", "_", "w", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "b", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can exclude wall-blocked attacks", ->
                        moves = new MoveTree({ row: 5, col: 4 })

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 5, 4, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("queens attacking their own pieces", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "b", "_", "_", "_", "b", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "B", "_", "_", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "b", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("cannot do it", ->
                        moves = new MoveTree({ row: 5, col: 4 })

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 5, 4, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("queen attack chaining and branching", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "w", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "w", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "w", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "w", "_", "_" ],
                                [ "B", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can chain attacks (any distance)", ->
                        model.set(forceMaxAttack: false)

                        moves = new MoveTree({ row: 1, col: 1 })
                        moves.addTree(new MoveTree({ row: 4, col: 4 }, { row: 3, col: 3 }))
                        moves.next[0].addTree(new MoveTree({ row: 1, col: 7 }, { row: 2, col: 6 }))
                        moves.addTree(new MoveTree({ row: 5, col: 5 }, { row: 3, col: 3 }))
                        moves.next[1].addTree(new MoveTree({ row: 8, col: 2 }, { row: 7, col: 3 }))
                        moves.addTree(new MoveTree({ row: 6, col: 6 }, { row: 3, col: 3 }))
                        moves.next[2].addTree(new MoveTree({ row: 8, col: 4 }, { row: 7, col: 5 }))
                        moves.next[2].next[0].addTree(new MoveTree({ row: 6, col: 2 }, { row: 7, col: 3 }))
                        moves.next[2].next[0].next[0].addTree(new MoveTree({ row: 1, col: 7 }, { row: 2, col: 6 }))
                        moves.addTree(new MoveTree({ row: 7, col: 7 }, { row: 3, col: 3 }))
                        moves.addTree(new MoveTree({ row: 8, col: 8 }, { row: 3, col: 3 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 1, 1, null)
                        expect(result).toEqual(moves)
                    )
                    it("can chain attacks (max distance)", ->
                        model.set(forceMaxAttack: true)

                        moves = new MoveTree({ row: 1, col: 1 })
                        moves.addTree(new MoveTree({ row: 6, col: 6 }, { row: 3, col: 3 }))
                        moves.next[0].addTree(new MoveTree({ row: 8, col: 4 }, { row: 7, col: 5 }))
                        moves.next[0].next[0].addTree(new MoveTree({ row: 6, col: 2 }, { row: 7, col: 3 }))
                        moves.next[0].next[0].next[0].addTree(new MoveTree({ row: 1, col: 7 }, { row: 2, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 1, 1, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("queen attack range", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "B", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            whiteOnTop: true
                        )
                    )

                    it("can find attack moves (restricted range)", ->
                        model.set(freeQueenRange: false)

                        moves = new MoveTree({ row: 3, col: 3 })
                        moves.addTree(new MoveTree({ row: 1, col: 1 }, { row: 2, col: 2 }))

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 3, 3, null)
                        expect(result).toEqual(moves)
                    )
                    it("can find attack moves (free range)", ->
                        model.set(freeQueenRange: true)

                        moves = new MoveTree({ row: 3, col: 3 })
                        moves.addTree(new MoveTree({ row: 1, col: 1 }, { row: 2, col: 2 }))
                        moves.addTree(new MoveTree({ row: 8, col: 8 }, { row: 7, col: 7 }))

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 3, 3, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("attacks over dead pieces", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "w", "_", "_" ],
                                [ "_", "_", "w", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "B", "_", "_" ],
                                [ "_", "_", "w", "_", "w", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "w", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            forceMaxAttack: true
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can prevent attacks over dead pieces", ->
                        moves = new MoveTree({ row: 5, col: 6 })
                        moves.addTree(new MoveTree({ row: 3, col: 4 }, { row: 4, col: 5 }))
                        moves.next[0].addTree(new MoveTree({ row: 5, col: 2 }, { row: 4, col: 3 }))
                        moves.next[0].next[0].addTree(new MoveTree({ row: 8, col: 5 }, { row: 6, col: 3 }))
                        moves.next[0].next[0].next[0].addTree(new MoveTree({ row: 6, col: 7 }, { row: 7, col: 6 }))
                        moves.next[0].next[0].next[0].addTree(new MoveTree({ row: 5, col: 8 }, { row: 7, col: 6 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.QUEEN, 5, 6, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("piece mid-attack promotion rules", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "w", "_", "_", "_", "_" ],
                                [ "b", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can enforce no promotion variant", ->
                        model.set(midAttackPromotion: constants.midAttackPromotionRules.NO_PROMOTION)

                        moves = new MoveTree({ row: 6, col: 1 })
                        moves.addTree(new MoveTree({ row: 8, col: 3 }, { row: 7, col: 2 }))
                        moves.next[0].addTree(new MoveTree({ row: 6, col: 5 }, { row: 7, col: 4 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 6, 1, null)
                        expect(result).toEqual(moves)
                    )
                    it("can enforce promote and stop variant", ->
                        model.set(midAttackPromotion: constants.midAttackPromotionRules.PROMOTE_AND_STOP)

                        moves = new MoveTree({ row: 6, col: 1 })
                        moves.addTree(new MoveTree({ row: 8, col: 3 }, { row: 7, col: 2 }))

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 6, 1, null)
                        expect(result).toEqual(moves)
                    )
                    it("can enforce promote and continue variant", ->
                        model.set(midAttackPromotion: constants.midAttackPromotionRules.PROMOTE_AND_CONTINUE)

                        moves = new MoveTree({ row: 6, col: 1 })
                        moves.addTree(new MoveTree({ row: 8, col: 3 }, { row: 7, col: 2 }))
                        moves.next[0].addTree(new MoveTree({ row: 6, col: 5 }, { row: 7, col: 4 }))
                        moves.next[0].next[0].addTree(new MoveTree({ row: 3, col: 8 }, { row: 4, col: 7 }))
                        moves.next[0].addTree(new MoveTree({ row: 5, col: 6 }, { row: 7, col: 4 }))
                        moves.next[0].next[1].addTree(new MoveTree({ row: 3, col: 8 }, { row: 4, col: 7 }))
                        moves.relabelHeights()

                        result = model._getAttackMovesOfPiece(constants.players.BLACK,
                                constants.classes.CHECKER, 6, 1, null)
                        expect(result).toEqual(moves)
                    )
                )

                describe("attacking move reporting (men)", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "w", "_", "w", "_", "_" ],
                                [ "_", "_", "b", "_", "_", "_", "b", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            forceMaxAttack: false
                            whiteOnTop: true
                        )
                    )

                    it("can correctly report moves for upward-facing player", ->
                        moves = []
                        moves.push(new MoveTree({ row: 4, col: 3 }))
                        moves[0].addTree(new MoveTree({ row: 6, col: 1 }, { row: 5, col: 2 }))
                        moves[0].next[0].addTree(new MoveTree({ row: 8, col: 3 }, { row: 7, col: 2 }))
                        moves[0].addTree(new MoveTree({ row: 6, col: 5 }, { row: 5, col: 4 }))
                        moves.push(new MoveTree({ row: 4, col: 7 }))
                        moves[1].addTree(new MoveTree({ row: 6, col: 5 }, { row: 5, col: 6 }))
                        tree.relabelHeights() for tree in moves

                        result = model._getAttackMoves(constants.players.BLACK)
                        expect(result).toEqual(moves)
                    )
                    it("can correctly report moves for downward-facing player", ->
                        moves = []
                        moves.push(new MoveTree({ row: 5, col: 2 }))
                        moves[0].addTree(new MoveTree({ row: 3, col: 4 }, { row: 4, col: 3 }))
                        moves.push(new MoveTree({ row: 5, col: 4 }))
                        moves[1].addTree(new MoveTree({ row: 3, col: 2 }, { row: 4, col: 3 }))
                        moves.push(new MoveTree({ row: 5, col: 6 }))
                        moves[2].addTree(new MoveTree({ row: 3, col: 8 }, { row: 4, col: 7 }))

                        result = model._getAttackMoves(constants.players.WHITE)
                        expect(result).toEqual(moves)
                    )
                )

                describe("attacking move reporting (queens)", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "W", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "B", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "B", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "W", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            forceMaxAttack: false
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can correctly report moves for upward-facing player", ->
                        moves = []
                        moves.push(new MoveTree({ row: 4, col: 7 }))
                        moves[0].addTree(new MoveTree({ row: 1, col: 4 }, { row: 2, col: 5 }))
                        moves[0].addTree(new MoveTree({ row: 8, col: 3 }, { row: 7, col: 4 }))
                        moves.push(new MoveTree({ row: 5, col: 2 }))
                        moves[1].addTree(new MoveTree({ row: 1, col: 6 }, { row: 2, col: 5 }))
                        moves[1].addTree(new MoveTree({ row: 8, col: 5 }, { row: 7, col: 4 }))

                        result = model._getAttackMoves(constants.players.BLACK)
                        expect(result).toEqual(moves)
                    )
                    it("can correctly report moves for downward-facing player", ->
                        moves = []
                        moves.push(new MoveTree({ row: 2, col: 5 }))
                        moves[0].addTree(new MoveTree({ row: 6, col: 1 }, { row: 5, col: 2 }))
                        moves[0].addTree(new MoveTree({ row: 5, col: 8 }, { row: 4, col: 7 }))
                        moves.push(new MoveTree({ row: 7, col: 4 }))
                        moves[1].addTree(new MoveTree({ row: 4, col: 1 }, { row: 5, col: 2 }))
                        moves[1].addTree(new MoveTree({ row: 3, col: 8 }, { row: 4, col: 7 }))

                        result = model._getAttackMoves(constants.players.WHITE)
                        expect(result).toEqual(moves)
                    )
                )
            )


            describe("other move calculation cases", ->
                it("will not infinitely recurse if there is an attack loop", ->
                    model.set(
                        board: [
                            [ "_", "_", "_", "_", "_", "_" ],
                            [ "_", "_", "_", "_", "_", "_" ],
                            [ "_", "w", "_", "w", "_", "_" ],
                            [ "_", "_", "_", "_", "_", "_" ],
                            [ "_", "w", "_", "w", "_", "_" ],
                            [ "_", "_", "_", "_", "_", "_" ]
                        ].reverse()
                        boardSize: 6
                        normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
                        whiteOnTop: true
                    )

                    moves = new MoveTree({ row: 1, col: 3 })
                    moves.addTree(new MoveTree({ row: 3, col: 1 }, { row: 2, col: 2 }))
                    moves.next[0].addTree(new MoveTree({ row: 5, col: 3 }, { row: 4, col: 2 }))
                    moves.next[0].next[0].addTree(new MoveTree({ row: 3, col: 5 }, { row: 4, col: 4 }))
                    moves.next[0].next[0].next[0].addTree(new MoveTree({ row: 1, col: 3 }, { row: 2, col: 4 }))
                    moves.addTree(new MoveTree({ row: 3, col: 5 }, { row: 2, col: 4 }))
                    moves.next[1].addTree(new MoveTree({ row: 5, col: 3 }, { row: 4, col: 4 }))
                    moves.next[1].next[0].addTree(new MoveTree({ row: 3, col: 1 }, { row: 4, col: 2 }))
                    moves.next[1].next[0].next[0].addTree(new MoveTree({ row: 1, col: 3 }, { row: 2, col: 2 }))
                    moves.relabelHeights()

                    result = model._getAttackMovesOfPiece(constants.players.BLACK, constants.classes.CHECKER, 1, 3, null)
                    expect(result).toEqual(moves)
                )

                it("will return attack moves if attacks can be made", ->
                    model.set(
                        board: [
                            [ "_", "_", "_", "_", "_" ],
                            [ "_", "_", "_", "_", "_" ],
                            [ "_", "_", "_", "_", "b" ],
                            [ "_", "w", "_", "_", "_" ],
                            [ "_", "_", "b", "_", "_" ]
                        ].reverse()
                        boardSize: 5
                        curPlayer: constants.players.BLACK
                        whiteOnTop: true
                    )

                    moves = []
                    moves.push(new MoveTree({ row: 1, col: 3 }, null))
                    moves[0].addTree(new MoveTree({ row: 3, col: 1 }, { row: 2, col: 2 }))

                    model._computeAvailableMoves()
                    expect(model.availableMoves).toEqual(moves)
                )
                it("will return normal moves if attacks cannot be made", ->
                    model.set(
                        board: [
                            [ "_", "_", "_", "_", "_" ],
                            [ "_", "_", "_", "_", "_" ],
                            [ "w", "_", "_", "_", "b" ],
                            [ "_", "_", "_", "_", "_" ],
                            [ "_", "_", "b", "_", "_" ]
                        ].reverse()
                        boardSize: 5
                        curPlayer: constants.players.BLACK
                        whiteOnTop: true
                    )

                    moves = []
                    moves.push(new MoveTree({ row: 1, col: 3 }, null))
                    moves[0].addTree(new MoveTree({ row: 2, col: 2 }, null))
                    moves[0].addTree(new MoveTree({ row: 2, col: 4 }, null))
                    moves.push(new MoveTree({ row: 3, col: 5 }, null))
                    moves[1].addTree(new MoveTree({ row: 4, col: 4 }, null))

                    model._computeAvailableMoves()
                    expect(model.availableMoves).toEqual(moves)
                )
            )

            describe("move making logic", ->
                describe("basic error tests", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "w", "_", "w", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "b", "_", "b", ]
                            ].reverse()
                            boardSize: 4
                            curPlayer: constants.players.BLACK
                            whiteOnTop: true
                        )
                        model._computeAvailableMoves()
                    )
                    
                    it("throws on invalid move sequence (empty)", ->
                        moveSeq = []
                        expect(-> model.makeMove(moveSeq)).toThrow()
                    )
                    it("throws on invalid move sequence (length 1)", ->
                        moveSeq = [ { row: 1, col: 1 } ]
                        expect(-> model.makeMove(moveSeq)).toThrow()
                    )
                    it("throws on invalid move sequence (non-existent)", ->
                        moveSeq = [ { row: 1, col: 2 }, { row: 2, col: 2 } ]
                        expect(-> model.makeMove(moveSeq)).toThrow()
                    )
                )

                describe("normal piece moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "w", "_", "W", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "B", "_", "b", ]
                            ].reverse()
                            boardSize: 4
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can move a man of an upward-facing player", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 4 }, { row: 2, col: 3 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(1, 4)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(2, 3)).toEqual(constants.pieces.BLACK_CHECKER)
                    )
                    it("can move a man of a downward-facing player", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 4, col: 1 }, { row: 3, col: 2 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(4, 1)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(3, 2)).toEqual(constants.pieces.WHITE_CHECKER)
                    )

                    it("can move a queen of an upward-facing player", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 3, col: 4 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(1, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(3, 4)).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                    it("can move a queen of a downward-facing player", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 4, col: 3 }, { row: 2, col: 1 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(4, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(2, 1)).toEqual(constants.pieces.WHITE_QUEEN)
                    )
                )

                describe("attack man moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "w", "_", "_", "_", "_", "_", "w", "_", ],
                                [ "_", "b", "_", "_", "_", "b", "_", "_", ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", ],
                                [ "_", "b", "_", "_", "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", "_", "_", "w", "_", ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", ],
                                [ "_", "_", "w", "_", "_", "_", "w", "_", ],
                                [ "_", "b", "_", "_", "_", "_", "_", "b", ]
                            ].reverse()
                            boardSize: 8
                            forceMaxAttack: false
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can make a single man attack for an upward-facing player", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 3, col: 4 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(1, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(2, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(3, 4)).toEqual(constants.pieces.BLACK_CHECKER)
                    )
                    it("can make a chained man attack for an upward-facing player", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 8 }, { row: 3, col: 6 }, { row: 5, col: 8 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(1, 8)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(2, 7)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(3, 6)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(4, 7)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(5, 8)).toEqual(constants.pieces.BLACK_CHECKER)
                    )

                    it("can make a single man attack for a downward-facing player", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 8, col: 7 }, { row: 6, col: 5 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(8, 7)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 6)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(6, 5)).toEqual(constants.pieces.WHITE_CHECKER)
                    )
                    it("can make a chained man attack for a downward-facing player", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 8, col: 1 }, { row: 6, col: 3 }, { row: 4, col: 1 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(8, 1)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(6, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(5, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(4, 1)).toEqual(constants.pieces.WHITE_CHECKER)
                    )

                    it("throws if a full attack sequence is not specified", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 8 }, { row: 3, col: 6 } ]
                        expect(-> model.makeMove(moveSeq)).toThrow()
                    )
                )

                describe("attack queen moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "W", "_", ],
                                [ "_", "w", "_", "_", "_", "_", "_", "_", ],
                                [ "_", "_", "B", "_", "_", "_", "_", "_", ],
                                [ "_", "b", "_", "b", "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", "w", "_", "w", "_", ],
                                [ "_", "_", "_", "_", "_", "W", "_", "_", ],
                                [ "_", "_", "_", "_", "_", "_", "b", "_", ],
                                [ "_", "B", "_", "_", "_", "_", "_", "_", ]
                            ].reverse()
                            boardSize: 8
                            forceMaxAttack: false
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can make a single queen attack for an upward-facing player", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 6, col: 3 }, { row: 8, col: 1 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(6, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(8, 1)).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                    it("can make a chained queen attack for an upward-facing player", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 5, col: 6 }, { row: 3, col: 8 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(1, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(2, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(3, 4)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(4, 5)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(5, 6)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(4, 7)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(3, 8)).toEqual(constants.pieces.BLACK_QUEEN)
                    )

                    it("can make a single queen attack for a downward-facing player", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 3, col: 6 }, { row: 1, col: 8 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(3, 6)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(2, 7)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(1, 8)).toEqual(constants.pieces.WHITE_QUEEN)
                    )
                    it("can make a chained queen attack for a downward-facing player", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 8, col: 7 }, { row: 4, col: 3 }, { row: 6, col: 1 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(8, 7)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 6)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(6, 5)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(5, 4)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(4, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(5, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(6, 1)).toEqual(constants.pieces.WHITE_QUEEN)
                    )

                    it("throws if a full attack sequence is not specified", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 5, col: 6 } ]
                        expect(-> model.makeMove(moveSeq)).toThrow()
                    )
                )

                describe("piece promotion moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_" ],
                                [ "_", "_", "_", "b" ],
                                [ "w", "_", "_", "_" ],
                                [ "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 4
                            whiteOnTop: true
                        )
                    )

                    it("can promote men of upward-facing player", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 3, col: 4 }, { row: 4, col: 3 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(3, 4)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(4, 3)).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                    it("can promote men of downward-facing player", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 2, col: 1 }, { row: 1, col: 2 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(2, 1)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(1, 2)).toEqual(constants.pieces.WHITE_QUEEN)
                    )
                )

                describe("piece mid-attack promotion rule variants", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "w", "_", "w", "_", "_", "_", "_" ],
                                [ "b", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "w", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_" ]
                            ].reverse()
                            boardSize: 8
                            curPlayer: constants.players.BLACK,
                            freeQueenRange: true
                            whiteOnTop: true
                        )
                    )

                    it("can enforce no promotion variant", ->
                        model.set(midAttackPromotion: constants.midAttackPromotionRules.NO_PROMOTION)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 6, col: 1 }, { row: 8, col: 3 }, { row: 6, col: 5 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(6, 1)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(8, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 4)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(6, 5)).toEqual(constants.pieces.BLACK_CHECKER)
                    )
                    it("can enforce promote and stop variant", ->
                        model.set(midAttackPromotion: constants.midAttackPromotionRules.PROMOTE_AND_STOP)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 6, col: 1 }, { row: 8, col: 3 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(6, 1)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(8, 3)).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                    it("can enforce promote and continue variant", ->
                        model.set(midAttackPromotion: constants.midAttackPromotionRules.PROMOTE_AND_CONTINUE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 6, col: 1 }, { row: 8, col: 3 }, { row: 6, col: 5 },
                            { row: 3, col: 8 } ]
                        model.makeMove(moveSeq)

                        expect(model.getCellContents(6, 1)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 2)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(8, 3)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(7, 4)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(6, 5)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(5, 6)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(4, 7)).toEqual(constants.pieces.EMPTY)
                        expect(model.getCellContents(3, 8)).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                )

                describe("piece counters after normal moves", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "w", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "b", "_", "_", ],
                            ].reverse()
                            blackCount: 1
                            boardSize: 4
                            whiteCount: 1
                            whiteOnTop: true
                        )
                    )

                    it("can update piece counters after upward-facing player's normal move", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 2, col: 3 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("blackCount")).toEqual(1)
                        expect(model.get("whiteCount")).toEqual(1)
                    )
                    it("can update piece counters after downward-facing player's normal move", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 4, col: 3 }, { row: 3, col: 2 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("blackCount")).toEqual(1)
                        expect(model.get("whiteCount")).toEqual(1)
                    )
                )

                describe("piece counters after attacks", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "w", "_", "_", "_", "_", "_", "w", "_", ],
                                [ "_", "b", "_", "_", "_", "b", "_", "_", ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", ],
                                [ "_", "b", "_", "_", "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", "_", "_", "w", "_", ],
                                [ "_", "_", "_", "_", "_", "_", "_", "_", ],
                                [ "_", "_", "w", "_", "_", "_", "w", "_", ],
                                [ "_", "b", "_", "_", "_", "_", "_", "b", ]
                            ].reverse()
                            blackCount: 6
                            boardSize: 8
                            forceMaxAttack: false
                            whiteCount: 6
                            whiteOnTop: true
                        )
                    )

                    it("can update piece counters after upward-facing player's single attack", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 3, col: 4 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("blackCount")).toEqual(6)
                        expect(model.get("whiteCount")).toEqual(5)
                    )
                    it("can update piece counters after upward-facing player's chained attack", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 8 }, { row: 3, col: 6 }, { row: 5, col: 8 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("blackCount")).toEqual(6)
                        expect(model.get("whiteCount")).toEqual(4)
                    )

                    it("can update piece counters after downward-facing player's single attack", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 8, col: 7 }, { row: 6, col: 5 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("blackCount")).toEqual(5)
                        expect(model.get("whiteCount")).toEqual(6)
                    )
                    it("can update piece counters after downward-facing player's chained attack", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 8, col: 1 }, { row: 6, col: 3 }, { row: 4, col: 1 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("blackCount")).toEqual(4)
                        expect(model.get("whiteCount")).toEqual(6)
                    )
                )

                describe("move counter update logic", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "w", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "b", "_", "_", ],
                            ].reverse()
                            boardSize: 4
                            turnNumber: 1
                            whiteOnTop: true
                        )
                    )

                    it("can update move counter (black move, black starts)", ->
                        model.set(
                            curPlayer: constants.players.BLACK
                            whiteStarts: false
                        )
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 2, col: 3 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("turnNumber")).toEqual(1)
                    )
                    it("can update move counter (black move, white starts)", ->
                        model.set(
                            curPlayer: constants.players.BLACK
                            whiteStarts: true
                        )
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 2, col: 3 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("turnNumber")).toEqual(2)
                    )
                    it("can update move counter (white move, black starts)", ->
                        model.set(
                            curPlayer: constants.players.WHITE
                            whiteStarts: false
                        )
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 4, col: 3 }, { row: 3, col: 4 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("turnNumber")).toEqual(2)
                    )
                    it("can update move counter (white move, white starts)", ->
                        model.set(
                            curPlayer: constants.players.WHITE
                            whiteStarts: true
                        )
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 4, col: 3 }, { row: 3, col: 4 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("turnNumber")).toEqual(1)
                    )
                )

                describe("player turn update logic", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "_", "_", "w", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "_", "_", "_", ],
                                [ "_", "b", "_", "_", ],
                            ].reverse()
                            boardSize: 4
                            whiteOnTop: true
                        )
                    )

                    it("can update current player (black move)", ->
                        model.set(curPlayer: constants.players.BLACK)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 1, col: 2 }, { row: 2, col: 3 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("curPlayer")).toEqual(constants.players.WHITE)
                    )
                    it("can update current player (white move)", ->
                        model.set(curPlayer: constants.players.WHITE)
                        model._computeAvailableMoves()

                        moveSeq = [ { row: 4, col: 3 }, { row: 3, col: 4 } ]
                        model.makeMove(moveSeq)

                        expect(model.get("curPlayer")).toEqual(constants.players.BLACK)
                    )
                )
            )

            describe("winning player check", ->
                it("can identify game in progress", ->
                    model.set( blackCount: 3, whiteCount: 4 )
                    expect(model.getWinner()).toEqual(constants.players.NONE)
                )
                it("can detect when white player has won", ->
                    model.set( blackCount: 0, whiteCount: 1, suicide: false )
                    expect(model.getWinner()).toEqual(constants.players.WHITE)
                )
                it("can detect when black player has won", ->
                    model.set( blackCount: 1, whiteCount: 0, suicide: false )
                    expect(model.getWinner()).toEqual(constants.players.BLACK)
                )
                it("can detect when white player has won (suicide variant)", ->
                    model.set( blackCount: 1, whiteCount: 0, suicide: true )
                    expect(model.getWinner()).toEqual(constants.players.WHITE)
                )
                it("can detect when black player has won (suicide variant)", ->
                    model.set( blackCount: 0, whiteCount: 1, suicide: true )
                    expect(model.getWinner()).toEqual(constants.players.BLACK)
                )
            )

            describe("utilities", ->
                describe("board containment check", ->
                    it("can identify coordinates inside the board", ->
                        expect(model.isCellOnBoard(4, 5)).toEqual(true)
                    )
                    it("can identify coordinates outside the board (row low)", ->
                        expect(model.isCellOnBoard(0, 5)).toEqual(false)
                    )
                    it("can identify coordinates outside the board (row high)", ->
                        expect(model.isCellOnBoard(9, 5)).toEqual(false)
                    )
                    it("can identify coordinates outside the board (column low)", ->
                        expect(model.isCellOnBoard(4, 0)).toEqual(false)
                    )
                    it("can identify coordinates outside the board (column high)", ->
                        expect(model.isCellOnBoard(4, 9)).toEqual(false)
                    )
                )

                describe("board content accessor", ->
                    it("can return contents of an empty, bright cell", ->
                        expect(model.getCellContents(1, 1)).toEqual(constants.pieces.EMPTY)
                    )
                    it("can return contents of an empty, dark cell", ->
                        expect(model.getCellContents(4, 5)).toEqual(constants.pieces.EMPTY)
                    )
                    it("can return contents of a cell with a black checker in it", ->
                        expect(model.getCellContents(2, 3)).toEqual(constants.pieces.BLACK_CHECKER)
                    )
                    it("can return contents of a cell with a white checker in it", ->
                        expect(model.getCellContents(7, 4)).toEqual(constants.pieces.WHITE_CHECKER)
                    )
                )

                describe("board content mutator", ->
                    it("can remove a checker from the board", ->
                        model._setCellContents(2, 1, constants.pieces.EMPTY)
                        expect(model.get("board")[1][0]).toEqual(constants.pieces.EMPTY)
                    )
                    it("can add a white checker to the board", ->
                        model._setCellContents(4, 3, constants.pieces.WHITE_CHECKER)
                        expect(model.get("board")[3][2]).toEqual(constants.pieces.WHITE_CHECKER)
                    )
                    it("can add a black checker to the board", ->
                        model._setCellContents(4, 3, constants.pieces.BLACK_CHECKER)
                        expect(model.get("board")[3][2]).toEqual(constants.pieces.BLACK_CHECKER)
                    )
                    it("can add a white queen to the board", ->
                        model._setCellContents(4, 3, constants.pieces.WHITE_QUEEN)
                        expect(model.get("board")[3][2]).toEqual(constants.pieces.WHITE_QUEEN)
                    )
                    it("can add a black queen to the board", ->
                        model._setCellContents(4, 3, constants.pieces.BLACK_QUEEN)
                        expect(model.get("board")[3][2]).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                )

                describe("piece ownership check", ->
                    it("can identify empty cells", ->
                        expect(model.getPieceOwnership(1, 1)).toEqual(constants.players.NONE)
                    )
                    it("can correctly assign white checkers", ->
                        expect(model.getPieceOwnership(8, 1)).toEqual(constants.players.WHITE)
                    )
                    it("can correctly assign white queens", ->
                        model._setCellContents(8, 1, constants.pieces.WHITE_QUEEN)
                        expect(model.getPieceOwnership(8, 1)).toEqual(constants.players.WHITE)
                    )
                    it("can correctly assign black checkers", ->
                        expect(model.getPieceOwnership(1, 8)).toEqual(constants.players.BLACK)
                    )
                    it("can correctly assign black queens", ->
                        model._setCellContents(1, 8, constants.pieces.BLACK_QUEEN)
                        expect(model.getPieceOwnership(1, 8)).toEqual(constants.players.BLACK)
                    )
                )

                describe("piece class check", ->
                    it("can identify empty cells", ->
                        expect(model.getPieceClass(5, 5)).toEqual(constants.classes.EMPTY)
                    )
                    it("can correctly assign white checkers", ->
                        expect(model.getPieceClass(7, 6)).toEqual(constants.classes.CHECKER)
                    )
                    it("can correctly assign black checkers", ->
                        expect(model.getPieceClass(1, 4)).toEqual(constants.classes.CHECKER)
                    )
                    it("can correctly assign white queens", ->
                        model._setCellContents(7, 6, constants.pieces.WHITE_QUEEN)
                        expect(model.getPieceClass(7, 6)).toEqual(constants.classes.QUEEN)
                    )
                    it("can correctly assign black queens", ->
                        model._setCellContents(1, 4, constants.pieces.BLACK_QUEEN)
                        expect(model.getPieceClass(1, 4)).toEqual(constants.classes.QUEEN)
                    )
                )

                describe("getter of specific piece", ->
                    it("can correctly return a white checker", ->
                        value = model.getSpecificPiece(constants.players.WHITE, constants.classes.CHECKER)
                        expect(value).toEqual(constants.pieces.WHITE_CHECKER)
                    )
                    it("can correctly return a white queen", ->
                        value = model.getSpecificPiece(constants.players.WHITE, constants.classes.QUEEN)
                        expect(value).toEqual(constants.pieces.WHITE_QUEEN)
                    )
                    it("can correctly return a black checker", ->
                        value = model.getSpecificPiece(constants.players.BLACK, constants.classes.CHECKER)
                        expect(value).toEqual(constants.pieces.BLACK_CHECKER)
                    )
                    it("can correctly return a black queen", ->
                        value = model.getSpecificPiece(constants.players.BLACK, constants.classes.QUEEN)
                        expect(value).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                )

                describe("last row calculation function", ->
                    it("can return last row for white player when it is on top", ->
                        model.set( whiteOnTop: true )
                        expect(model._getPlayerLastRow(constants.players.WHITE)).toEqual(1)
                    )
                    it("can return last row for white player when it is on bottom", ->
                        model.set( whiteOnTop: false )
                        expect(model._getPlayerLastRow(constants.players.WHITE)).toEqual(8)
                    )
                    it("can return last row for black player when it is on top", ->
                        model.set( whiteOnTop: false )
                        expect(model._getPlayerLastRow(constants.players.BLACK)).toEqual(1)
                    )
                    it("can return last row for black player when it is on bottom", ->
                        model.set( whiteOnTop: true )
                        expect(model._getPlayerLastRow(constants.players.BLACK)).toEqual(8)
                    )
                )

                describe("player direction calculation function", ->
                    it("can return direction for white player when it is on top", ->
                        model.set( whiteOnTop: true )
                        expect(model._getPlayerYForwardDir(constants.players.WHITE)).toEqual(-1)
                    )
                    it("can return direction for white player when it is on bottom", ->
                        model.set( whiteOnTop: false )
                        expect(model._getPlayerYForwardDir(constants.players.WHITE)).toEqual(1)
                    )
                    it("can return direction for black player when it is on top", ->
                        model.set( whiteOnTop: false )
                        expect(model._getPlayerYForwardDir(constants.players.BLACK)).toEqual(-1)
                    )
                    it("can return direction for black player when it is on bottom", ->
                        model.set( whiteOnTop: true )
                        expect(model._getPlayerYForwardDir(constants.players.BLACK)).toEqual(1)
                    )
                )

                describe("piece promotion function", ->
                    beforeEach( ->
                        model.set(
                            board: [
                                [ "w", "_", "W", "_" ],
                                [ "_", "_", "_", "_" ],
                                [ "_", "_", "_", "_" ],
                                [ "_", "b", "_", "B" ],
                            ].reverse()
                            boardSize: 4
                            whiteOnTop: true
                        )
                    )

                    it("does nothing when a blank cell is specified", ->
                        model._promotePiece(1, 1)
                        expect(model.getCellContents(1, 1)).toEqual(constants.pieces.EMPTY)
                    )
                    it("correctly promotes a black man", ->
                        model._promotePiece(1, 2)
                        expect(model.getCellContents(1, 2)).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                    it("correctly promotes a white man", ->
                        model._promotePiece(4, 1)
                        expect(model.getCellContents(4, 1)).toEqual(constants.pieces.WHITE_QUEEN)
                    )
                    it("does nothing when a black queen is specified", ->
                        model._promotePiece(1, 4)
                        expect(model.getCellContents(1, 4)).toEqual(constants.pieces.BLACK_QUEEN)
                    )
                    it("does nothing when a white queen is specified", ->
                        model._promotePiece(4, 3)
                        expect(model.getCellContents(4, 3)).toEqual(constants.pieces.WHITE_QUEEN)
                    )
                )
            )

        )
)
