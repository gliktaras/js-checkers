({
    appDir: ".",
    baseUrl: "js",
    dir: "../checkers-build",

    fileExclusionRegExp: /(^\.|^coffee|^sass|^tests)/,

    inlineText: true,
    keepBuildDir: false,
    optimize: "uglify",

    modules: [
        {
            name: "main"
        }
    ],

    paths: {
        backbone: "lib/backbone-min",
        jquery: "lib/jquery-1.7.2.min",
        underscore: "lib/underscore-min"
    }
})
