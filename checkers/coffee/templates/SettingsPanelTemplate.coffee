# SettingsPanelTemplate.coffee
# A template for the settings panel.

define( ->
    """
    <h1>Rules</h1>
    <div class="rules">
        <div class="line">
            <label for="rule-presets">Preset</label>
            <select id="rule-presets"></select>
        </div>
        <div class="spacer"></div>
        <div class="line">
            <label for="board-size-field">Board size</label>
            <input type="text" id="board-size-field">
        </div>
        <div class="line">
            <label for="piece-rows-field">Rows to fill</label>
            <input type="text" id="piece-rows-field">
        </div>
        <div class="spacer"></div>
        <div class="line">
            <label for="attack-rules">Piece attack rules</label>
        </div>
        <div class="line">
            <select id="attack-rules">
                <option value="FORWARD_ONLY">Forward only</option>
                <option value="FORWARD_START_ONLY">First attack forward</option>
                <option value="ANY_DIRECTION">Any direction</option>
            </select>
        </div>
        <div class="line">
            <label for="promotion-rules">Mid-attack promotion</label>
        </div>
        <div class="line">
            <select id="promotion-rules">
                <option value="NO_PROMOTION">No promotion</option>
                <option value="PROMOTE_AND_STOP">Promote and stop attack</option>
                <option value="PROMOTE_AND_CONTINUE">Promote and continue move</option>
            </select>
        </div>
        <div class="spacer"></div>
        <div class="line">
            <input type="checkbox" id="max-attacks-checkbox">
            <label for="max-attacks-checkbox">Force maximum attacks</label>
        </div>
        <div class="line">
            <input type="checkbox" id="queen-distance-checkbox">
            <label for="queen-distance-checkbox">Queens move any distance</label>
        </div>
        <div class="spacer"></div>
        <div class="line">
            <div class="error-message"></div>
        </div>
        <div class="line">
            <button type="button" id="apply-button">Apply rules</button>
        </div>
    </div>
    <h1>Options</h1>
    <div id="options">
        <div class="line">
            <input type="checkbox" id="suicide-checkers-checkbox">
            <label for="suicide-checkers-checkbox">Suicide checkers</label>
        </div>
        <div class="line">
            <input type="checkbox" id="white-starts-checkbox">
            <label for="white-starts-checkbox">White start</label>
        </div>
        <div class="line">
            <input type="checkbox" id="white-on-top-checkbox">
            <label for="white-on-top-checkbox">White on top</label>
        </div>
        <div class="spacer"></div>
        <div class="line">
            <button type="button" id="reset-button">Start new game</button>
        </div>
    </div>
    """
)
