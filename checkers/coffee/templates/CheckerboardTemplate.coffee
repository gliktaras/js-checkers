# CheckerboardTemplate.coffee
# Code that generates the checkerboard DOM.

define(
    ["jquery", "common/utils"],
    ($, utils) ->

        (boardSize) ->
            table = $("<table></table>").append("<tbody></tbody>")

            for row in [1..boardSize]
                tableRow = $("<tr></tr>")
                tableRow.append( $("<td>#{row}</td>").addClass("board-label board-row-label") )

                for col in [1..boardSize]
                    cellDark = utils.isCellDark(row, col)

                    tableCell = $("<td></td>").attr("id", "cell-#{row}-#{col}").addClass("cell")
                    tableCell.addClass( if cellDark then "cell-dark" else "cell-bright" )

                    if row is 1 then tableCell.addClass("edge-bottom")
                    if row is boardSize then tableCell.addClass("edge-top")
                    if col is 1 then tableCell.addClass("edge-left")
                    if col is boardSize then tableCell.addClass("edge-right")

                    tableRow.append(tableCell)

                table.prepend(tableRow)

            tableRow = $("<tr></tr>")
            tableRow.append( $("<td></td>").addClass("board-corner") )

            for col in [1..boardSize]
                label = utils.getColumnLabel(col)
                tableRow.append( $("<td>#{label}</td>").addClass("board-label board-col-label") )

            table.append(tableRow)

            tableHtml = "<table>" + table.html() + "</table>"
            tableHtml
)
