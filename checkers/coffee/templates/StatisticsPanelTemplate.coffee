# StatisticsPanelTemplate.coffee
# A template for the statistics panel.

define( ->
    """
    <div id="move-counter"></div>
    <div id="turn-tracker"></div>
    <div id="game-message" class="hidden"></div>
    """
)
