# main.coffee
# The entry point.

require.config(
    paths:
        backbone: "lib/backbone-min"
        jquery: "lib/jquery-1.7.2.min"
        underscore: "lib/underscore-min"
)

require(
    ["jquery", "models/GameModel", "views/GameStatView", "views/GameView", "views/SettingsView"],
    ($, GameModel, GameStatView, GameView, SettingsView) ->
        gameModel = new GameModel()

        gameView = new GameView({ el: "#board", model: gameModel })
        gameStatView = new GameStatView({ el: "#board-stats", model: gameModel })
        settingsView = new SettingsView({ el: "#settings", gameView: gameView, model: gameModel })
)
