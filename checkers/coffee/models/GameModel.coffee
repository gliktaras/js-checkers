# GameModel.coffee
# Defines the model of the checkers game.

# Convention: make all modifications silently and trigger a change manually.
# This is needed because the board has to be modified during available move
# calculation routine and I want to keep the data handling consistent.

define(
    ["jquery", "underscore", "backbone", "common/constants", "common/utils", "common/MoveTree"],
    ($, _, Backbone, constants, utils, MoveTree) ->

        class GameModel extends Backbone.Model

            #--- INITIALISATION ------------------------------------------------------

            # Default model values.
            defaults:
                boardSize: 8
                forceMaxAttack: false
                freeQueenRange: true
                midAttackPromotion: constants.midAttackPromotionRules.PROMOTE_AND_STOP
                normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
                pieceRows: 3
                suicide: false
                whiteStarts: true
                whiteOnTop: true

            # Initialisation logic.
            initialize: =>
                @resetGameState()


            #--- AVAILABLE MOVES -----------------------------------------------------

            # Compute all available moves for the current turn.
            _computeAvailableMoves: =>
                @availableMoves = @_getAttackMoves( @get("curPlayer") )
                if @availableMoves.length is 0 then @availableMoves = @_getNormalMoves( @get("curPlayer") )


            # Get all valid normal moves for the given player.
            _getNormalMoves: (player) =>
                moves = []
                for row in [1..@get("boardSize")]
                    for col in [1..@get("boardSize")]
                        if @getPieceOwnership(row, col) is player
                            curMoves = @_getNormalMovesOfPiece(player, row, col)
                            moves.push(curMoves) unless curMoves.isLeaf()
                moves

            # Get all valid normal moves of a particular piece.
            _getNormalMovesOfPiece: (player, row, col) =>
                yForwardDir = @_getPlayerYForwardDir(player)

                if @getPieceClass(row, col) is constants.classes.CHECKER
                    moveDist = 1
                    dirs = [ { y: yForwardDir, x: -1 }, { y: yForwardDir, x: 1 } ]
                else
                    moveDist = if @get("freeQueenRange") then @get("boardSize") else 1
                    dirs = [ { y: -1, x: -1 }, { y: -1, x: 1 }, { y: 1, x: -1 }, { y: 1, x: 1 } ]

                moves = new MoveTree({ row: row, col: col }, null)

                for dir in dirs
                    curDist = 1
                    [y, x] = [row, col]

                    while curDist <= moveDist
                        [y, x] = [y + dir.y, x + dir.x]
                        if not @isCellOnBoard(y, x) then break

                        curOwner = @getPieceOwnership(y, x)
                        if curOwner is constants.players.NONE
                            moves.addTree(new MoveTree({ row: y, col: x }, null))
                            ++curDist
                        else
                            break

                moves


            # Get all valid attack moves for the given player.
            _getAttackMoves: (player) =>
                moves = []

                for row in [1..@get("boardSize")]
                    for col in [1..@get("boardSize")]
                        if @getPieceOwnership(row, col) is player
                            pieceClass = @getPieceClass(row, col)
                            curPiece = @getCellContents(row, col)

                            @_setCellContents(row, col, constants.pieces.EMPTY)
                            curMoves = @_getAttackMovesOfPiece(player, pieceClass, row, col, null)

                            if not curMoves.isLeaf()
                                if @get("forceMaxAttack")
                                    if moves.length > 0
                                        if moves[0].height < curMoves.height
                                            moves = []
                                            moves.push(curMoves)
                                        else if moves[0].height is curMoves.height
                                            moves.push(curMoves)
                                    else
                                        moves.push(curMoves)
                                else
                                    moves.push(curMoves)

                            @_setCellContents(row, col, curPiece)

                moves

            # Get all valid attack moves of a particular piece.
            _getAttackMovesOfPiece: (player, pieceClass, row, col, prevKill) =>
                enemy = utils.getOtherPlayer(player)
                lastRowIndex = @_getPlayerLastRow(player)
                yForwardDir = @_getPlayerYForwardDir(player)

                if pieceClass is constants.classes.CHECKER
                    attackDist = 1
                    dirs = [ { y: yForwardDir, x: -1 }, { y: yForwardDir, x: 1 } ]
                    if (@get("normalAttackDir") is constants.normalAttackRules.ANY_DIRECTION) or
                            (@get("normalAttackDir") is constants.normalAttackRules.FORWARD_START_ONLY and prevKill isnt null)
                        dirs = dirs.concat([ { y: -yForwardDir, x: -1 }, { y: -yForwardDir, x: 1 } ])
                else
                    attackDist = if @get("freeQueenRange") then @get("boardSize") else attackDist = 1
                    dirs = [ { y: -1, x: -1 }, { y: -1, x: 1 }, { y: 1, x: -1 }, { y: 1, x: 1 } ]

                moves = new MoveTree({ row: row, col: col }, prevKill)

                for dir in dirs
                    curDist = 1
                    [y, x] = [row, col]
                    hasTarget = false

                    while curDist <= attackDist
                        [y, x] = [y + dir.y, x + dir.x]
                        if not @isCellOnBoard(y, x) then break

                        curOwner = @getPieceOwnership(y, x)
                        if curOwner is enemy
                            hasTarget = true
                            break
                        else if curOwner is player
                            break
                        else
                            ++curDist

                    if not hasTarget then continue

                    curKill = { row: y, col: x }
                    curDeadPiece = @getCellContents(curKill.row, curKill.col)
                    @_setCellContents(curKill.row, curKill.col, @getSpecificPiece(player, pieceClass))

                    while curDist <= attackDist
                        [y, x] = [y + dir.y, x + dir.x]
                        if not @isCellOnBoard(y, x) then break

                        curOwner = @getPieceOwnership(y, x)
                        if curOwner is constants.players.NONE
                            nextClass = pieceClass

                            if y is lastRowIndex and pieceClass is constants.classes.CHECKER
                                if @get("midAttackPromotion") is constants.midAttackPromotionRules.PROMOTE_AND_STOP
                                    moves.addTree(new MoveTree({ row: y, col: x }, curKill))
                                    break
                                else if @get("midAttackPromotion") is constants.midAttackPromotionRules.PROMOTE_AND_CONTINUE
                                    nextClass = constants.classes.QUEEN

                            curMoves = @_getAttackMovesOfPiece(player, nextClass, y, x, curKill)

                            if @get("forceMaxAttack")
                                if moves.height <= curMoves.height
                                    moves.next = []
                                    moves.addTree(curMoves)
                                else if moves.height is curMoves.height + 1
                                    moves.addTree(curMoves)
                            else
                                moves.addTree(curMoves)

                            ++curDist
                        else
                            break

                    @_setCellContents(curKill.row, curKill.col, curDeadPiece)

                moves


            #--- GAME FLOW -----------------------------------------------------------

            # Get the game's winner, if there is one.
            getWinner: =>
                if @get("blackCount") > 0 and @get("whiteCount") > 0
                    return constants.players.NONE

                if @get("whiteCount") is 0
                    winner = constants.players.BLACK
                else
                    winner = constants.players.WHITE

                if @get("suicide")
                    winner = utils.getOtherPlayer(winner)

                winner


            # Make a move in the game.
            makeMove: (moveSeq) =>
                if moveSeq.length < 2
                    throw("Invalid move sequence passed (empty)")

                curPlayer = @get("curPlayer")
                enemyPlayer = utils.getOtherPlayer(curPlayer)

                lastRowIndex = @_getPlayerLastRow(curPlayer)

                firstMove = moveSeq[0]
                lastMove = moveSeq[moveSeq.length - 1]

                curPiece = @getCellContents(firstMove.row, firstMove.col)
                @_setCellContents(firstMove.row, firstMove.col, constants.pieces.EMPTY)

                if (lastMove.row is @_getPlayerLastRow(curPlayer))
                    if curPlayer is constants.players.WHITE
                        @_setCellContents(lastMove.row, lastMove.col, constants.pieces.WHITE_QUEEN)
                    else
                        @_setCellContents(lastMove.row, lastMove.col, constants.pieces.BLACK_QUEEN)

                curTreeNode = @availableMoves
                promotion = false

                for move in moveSeq
                    moveMatched = false

                    for nextNode in curTreeNode
                        if nextNode.own.row is move.row and nextNode.own.col is move.col
                            moveMatched = true

                            if nextNode.own.row is lastRowIndex
                                if (@get("midAttackPromotion") isnt constants.midAttackPromotionRules.NO_PROMOTION) or
                                        (nextNode.own.row is lastMove.row and nextNode.own.col is lastMove.col)
                                    promotion = true

                            if nextNode.kill isnt null
                                @_setCellContents(nextNode.kill.row, nextNode.kill.col, constants.pieces.EMPTY)
                                if enemyPlayer is constants.players.WHITE
                                    @attributes.whiteCount--
                                else
                                    @attributes.blackCount--

                            curTreeNode = nextNode.next
                            break

                    if not moveMatched
                        throw("Invalid move sequence passed (invalid move).")

                @_setCellContents(lastMove.row, lastMove.col, curPiece)
                if promotion
                    @_promotePiece(lastMove.row, lastMove.col)

                if curTreeNode.length > 0
                    throw("Invalid move sequence passed (incomplete attack).")

                nextTurnNumber = @get("turnNumber")

                if @get("whiteStarts")
                    if curPlayer is constants.players.BLACK then ++nextTurnNumber
                else
                    if curPlayer is constants.players.WHITE then ++nextTurnNumber

                @set({
                    curPlayer: enemyPlayer
                    turnNumber: nextTurnNumber
                }, { silent: true })
                @_computeAvailableMoves()

                @change()
            

            # Reset the board to starting state.
            resetGameState: =>
                [topChecker, bottomChecker] =
                    if @get("whiteOnTop")
                        [constants.pieces.WHITE_CHECKER, constants.pieces.BLACK_CHECKER]
                    else
                        [constants.pieces.BLACK_CHECKER, constants.pieces.WHITE_CHECKER]

                board =
                    for row in [1..@get("boardSize")]
                        for col in [1..@get("boardSize")]
                            if utils.isCellDark(row, col)
                                if row <= @get("pieceRows")
                                    bottomChecker
                                else if row > (@get("boardSize") - @get("pieceRows"))
                                    topChecker
                                else
                                    constants.pieces.EMPTY
                            else
                                constants.pieces.EMPTY

                pieceCount = Math.floor( @get("boardSize") / 2 ) * @get("pieceRows")
                if @get("boardSize") % 2 is 1
                    pieceCount += Math.floor( @get("pieceRows") / 2 )

                if @get("whiteStarts")
                    curPlayer = constants.players.WHITE
                else
                    curPlayer = constants.players.BLACK

                @set({
                    board: board
                    curPlayer: curPlayer
                    turnNumber: 1
                    blackCount: pieceCount
                    whiteCount: pieceCount
                }, { silent: true })
                @_computeAvailableMoves()

                @change()


            #--- UTILITY FUNCTIONS ---------------------------------------------------

            # Get the contents of the given board cell.
            getCellContents: (row, col) =>
                @get("board")[row - 1][col - 1]

            # Set the contents of the given board cell.
            _setCellContents: (row, col, value) =>
                @attributes.board[row - 1][col - 1] = value


            # Get the class of the piece in the given board cell.
            getPieceClass: (row, col) =>
                curCell = @getCellContents(row, col)
                if curCell is constants.pieces.WHITE_CHECKER or curCell is constants.pieces.BLACK_CHECKER
                    return constants.classes.CHECKER
                else if curCell is constants.pieces.WHITE_QUEEN or curCell is constants.pieces.BLACK_QUEEN
                    return constants.classes.QUEEN
                else
                    return constants.classes.EMPTY

            # Get the owner of the piece in the given cell.
            getPieceOwnership: (row, col) =>
                curCell = @getCellContents(row, col)
                if curCell is constants.pieces.WHITE_CHECKER or curCell is constants.pieces.WHITE_QUEEN
                    return constants.players.WHITE
                else if curCell is constants.pieces.BLACK_CHECKER or curCell is constants.pieces.BLACK_QUEEN
                    return constants.players.BLACK
                else
                    return constants.players.NONE

            # Get a specified piece symbol.
            getSpecificPiece: (player, klass) =>
                if player is constants.players.WHITE
                    if klass is constants.classes.CHECKER
                        return constants.pieces.WHITE_CHECKER
                    else if klass is constants.classes.QUEEN
                        return constants.pieces.WHITE_QUEEN
                else if player is constants.players.BLACK
                    if klass is constants.classes.CHECKER
                        return constants.pieces.BLACK_CHECKER
                    else if klass is constants.classes.QUEEN
                        return constants.pieces.BLACK_QUEEN


            # Check whether the given cell is on the board.
            isCellOnBoard: (row, col) =>
                (row >= 1) and (row <= @get("boardSize")) and (col >= 1) and (col <= @get("boardSize"))


            # Get the last index for the given player.
            _getPlayerLastRow: (player) =>
                lastRowIndex = 1
                if @get("whiteOnTop") and player is constants.players.BLACK then lastRowIndex = @get("boardSize")
                if not @get("whiteOnTop") and player is constants.players.WHITE then lastRowIndex = @get("boardSize")
                lastRowIndex

            # Get "forward" direction on the Y axis for the given player.
            _getPlayerYForwardDir: (player) =>
                yForwardDir = 1
                if @get("whiteOnTop") and player is constants.players.WHITE then yForwardDir = -1
                if not @get("whiteOnTop") and player is constants.players.BLACK then yForwardDir = -1
                yForwardDir


            # Promote the piece in the given cell, if possible.
            _promotePiece: (row, col) =>
                curCell = @getCellContents(row, col)
                if curCell is constants.pieces.WHITE_CHECKER
                    @_setCellContents(row, col, constants.pieces.WHITE_QUEEN)
                    return
                if curCell is constants.pieces.BLACK_CHECKER
                    @_setCellContents(row, col, constants.pieces.BLACK_QUEEN)
                    return


        GameModel
)
