# constants.coffee
# Constant definitions.

define(

    # Bound constants.
    minBoardSize: 4


    # Board state symbols.
    pieces:
        EMPTY: "_"
        BLACK_CHECKER: "b"
        BLACK_QUEEN: "B"
        WHITE_CHECKER: "w"
        WHITE_QUEEN: "W"

    # Piece class identifiers.
    classes:
        EMPTY: "none"
        CHECKER: "checker"
        QUEEN: "queen"

    # Player identifiers.
    players:
        NONE: "none"
        BLACK: "black"
        WHITE: "white"


    # Normal piece attack rules.
    normalAttackRules:
        FORWARD_ONLY: "FORWARD_ONLY"
        FORWARD_START_ONLY: "FORWARD_START_ONLY"
        ANY_DIRECTION: "ANY_DIRECTION"

    # Mid-attack piece promotion rules.
    midAttackPromotionRules:
        NO_PROMOTION: "NO_PROMOTION"
        PROMOTE_AND_STOP: "PROMOTE_AND_STOP"
        PROMOTE_AND_CONTINUE: "PROMOTE_AND_CONTINUE"

)
