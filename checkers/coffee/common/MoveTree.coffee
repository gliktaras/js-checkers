# MoveTree.coffee
# Defines a tree structure for representing checker moves.

define( ->

    class MoveTree

        # Create a new move tree instance.
        constructor: (own, kill = null) ->
            @own = own
            @kill = kill
            @height = 0
            @next = []

        # Add a tree of moves to the current node.
        addTree: (tree) ->
            @height = Math.max(@height, tree.height + 1)
            @next.push(tree)

        # Check whether the current node is a leaf.
        isLeaf: ->
            @height is 0

        # Recalculate the height values of the whole tree.
        relabelHeights: ->
            @height = 0
            for subTree in @next
                subTree.relabelHeights()
                @height = Math.max(@height, subTree.height + 1)

    MoveTree
)
