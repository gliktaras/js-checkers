# RulePresets.coffee
# All rule presets for the game.

define(
    [ "common/constants"],
    (constants) ->

        "english":
            fullRulesetName: "English/American"
            boardSize: 8
            forceMaxAttack: false
            freeQueenRange: false
            midAttackPromotion: constants.midAttackPromotionRules.PROMOTE_AND_STOP
            normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
            pieceRows: 3

        "international":
            fullRulesetName: "International"
            boardSize: 10
            forceMaxAttack: true
            freeQueenRange: true
            midAttackPromotion: constants.midAttackPromotionRules.PROMOTE_AND_CONTINUE
            normalAttackDir: constants.normalAttackRules.ANY_DIRECTION
            pieceRows: 4
)
