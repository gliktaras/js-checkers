# utils.coffee
# Various shared utility functions.

define(
    ["common/constants"],
    (constants) ->

        # Get a label for the given column.
        getColumnLabel: (col) ->
            return "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[ (col - 1) % 26 ]

        # Get a tree node with the given own coordinates.
        getMoveTreeNode: (nodeList, row, col) ->
            for node in nodeList
                if node.own.row is row and node.own.col is col
                    return node
            null

        # Get enemy of the given player.
        getOtherPlayer: (player) ->
            if player is constants.players.WHITE
                return constants.players.BLACK
            else if player is constants.players.BLACK
                return constants.players.WHITE
            else
                return constants.players.NONE

        # Get the sign of the given integer.
        getSign: (x) ->
            if x > 0
                return 1
            else if x < 0
                return -1
            else
                return 0

        # Check whether the given checkerboard square is dark.
        isCellDark: (row, col) ->
            (row + col) % 2
)
