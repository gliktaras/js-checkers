# SettingsView.coffee
# Defines the view of the settings pane.

define(
    ["jquery", "underscore", "backbone", "common/constants", "common/RulePresets", "templates/SettingsPanelTemplate"],
    ($, _, Backbone, constants, presets, template) ->

        class SettingsView extends Backbone.View

            # View's event mappings.
            events:
                "change #rule-presets": "_setRulesToCurrentPreset"

                "change #attack-rules": "_onFormInputChange"
                "change #promotion-rules": "_onFormInputChange"
                "change .rules input": "_onFormInputChange"
                "change .rules input[type='text']": "_onFormTextInputChange"

                "click #apply-button": "_onApplyRules"
                "click #reset-button": "_onNewGame"


            # Initialisation logic.
            initialize: =>
                @_createDOM()
                @_setRulesToCurrentPreset()
                @_setOptionsToCurrentModel()

            # DOM initialisation logic.
            _createDOM: =>
                @$el.html(template)
                for name, ruleset of presets
                    @$el.find("#rule-presets").append("<option value=\"#{name}\">#{ruleset.fullRulesetName}</option>")
                @$el.find("#rule-presets").append("<option value=\"custom\">Custom</option>")

            
            # Form interaction handler.
            _onFormInputChange: =>
                @$el.find("#rule-presets option[value=custom]").prop("selected", true)

            # Form interaction handler for text fields.
            _onFormTextInputChange: (e) =>
                target = $(e.currentTarget)
                if _.isNaN(parseInt(target.val(), 10))
                    target.closest("div").addClass("invalid")
                else
                    target.closest("div").removeClass("invalid")


            # Updates option values.
            _setOptionsToCurrentModel: =>
                @$el.find("#suicide-checkers-checkbox").prop("checked", @model.get('suicide'))
                @$el.find("#white-starts-checkbox").prop("checked", @model.get('whiteStarts'))
                @$el.find("#white-on-top-checkbox").prop("checked", @model.get('whiteOnTop'))

            # Rule preset change handler.
            _setRulesToCurrentPreset: =>
                curName = @$el.find("#rule-presets option:selected").attr("value")
                return if curName is "custom"

                curPreset = presets[curName]
                @$el.find("#attack-rules option[value=#{curPreset.normalAttackDir}]").prop("selected", true)
                @$el.find("#promotion-rules option[value=#{curPreset.midAttackPromotion}]").prop("selected", true)
                @$el.find("#board-size-field").val(curPreset.boardSize)
                @$el.find("#max-attacks-checkbox").prop("checked", curPreset.forceMaxAttack)
                @$el.find("#queen-distance-checkbox").prop("checked", curPreset.freeQueenRange)
                @$el.find("#piece-rows-field").val(curPreset.pieceRows)
                
                @$el.find(".invalid").removeClass("invalid")
                @$el.find(".error-message").text("")


            # Apply rules button click handler.
            _onApplyRules: =>
                if @$el.find(".invalid").length isnt 0
                    @$el.find(".error-message").text("The form contains errors")
                    return

                rules =
                    boardSize: parseInt( @$el.find("#board-size-field").val(), 10 )
                    forceMaxAttack: @$el.find("#max-attacks-checkbox").prop("checked")
                    freeQueenRange: @$el.find("#queen-distance-checkbox").prop("checked")
                    midAttackPromotion: @$el.find("#promotion-rules option:selected").attr("value")
                    normalAttackDir: @$el.find("#attack-rules option:selected").attr("value")
                    pieceRows: parseInt( @$el.find("#piece-rows-field").val(), 10 )
                @model.set(rules, {silent : true})

                @$el.find(".error-message").text("")
                @_onNewGame()

            # New game button click handler.
            _onNewGame: =>
                rules =
                    suicide: @$el.find("#suicide-checkers-checkbox").prop("checked")
                    whiteStarts: @$el.find("#white-starts-checkbox").prop("checked")
                    whiteOnTop: @$el.find("#white-on-top-checkbox").prop("checked")
                @model.set(rules, {silent : true})

                @model.resetGameState()


        SettingsView
)
