# GameStatView.coffee
# This view displays game's flow information - current turn, player etc.

define(
    ["jquery", "underscore", "backbone", "common/constants", "templates/StatisticsPanelTemplate"],
    ($, _, Backbone, constants, template) ->

        class GameStatView extends Backbone.View

            # Initialisation logic.
            initialize: () ->
                @$el.html(template)

                @model.on("change", @_updateState)
                @_updateState()


            # Update the panel's state.
            _updateState: =>
                playerString = if @model.get("curPlayer") is constants.players.WHITE then "WHITE" else "BLACK"

                if @model.getWinner() is constants.players.NONE
                    @$el.find("#move-counter").removeClass("hidden").text("Move: #{@model.get('turnNumber')}")
                    @$el.find("#turn-tracker").removeClass("hidden").text("Turn: #{playerString}")
                    @$el.find("#game-message").addClass("hidden")
                else
                    if @model.getWinner() is constants.players.WHITE
                        winText = "WHITE player wins"
                    else if @model.getWinner() is constants.players.BLACK
                        winText = "BLACK player wins"

                    @$el.find("#move-counter").addClass("hidden")
                    @$el.find("#turn-tracker").addClass("hidden")
                    @$el.find("#game-message").removeClass("hidden").text(winText)


        GameStatView
)
