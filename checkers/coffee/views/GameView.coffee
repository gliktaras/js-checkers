# GameView.coffee
# Defines the view of the checkerboard.

define(
    ["jquery", "underscore", "backbone", "common/constants", "common/utils", "templates/CheckerboardTemplate"],
    ($, _, Backbone, constants, utils, boardTemplate) ->

        class GameView extends Backbone.View

            # View's event mappings.
            events:
                "click .cell": "_onPieceClick"


            #--- INITIALISATION ------------------------------------------------------

            # Initialisation logic.
            initialize: () ->
                @_createBoardHtml()

                @_updateBoardState()
                @_resetCurMoveBuffer()

                @model.on("change:board", @_createBoardHtml)
                @model.on("change", @_updateView)

            # Generate the checkerboard's DOM tree.
            _createBoardHtml: =>
                boardHtml = boardTemplate(@model.get("boardSize"))
                @$el.html(boardHtml)


            #--- INPUT HANDLING ------------------------------------------------------

            # Board's click handler.
            _onPieceClick: (e) ->
                if @model.getWinner() is constants.players.NONE
                    [row, col] = @_getCellCoords($(e.currentTarget))

                    curNode = utils.getMoveTreeNode( @_curTreeTrace[@_curTreeTrace.length - 1], row, col )
                    if curNode is null
                        @_resetCurMoveBuffer()
                    else
                        @_curMoveSeq.push({ row: curNode.own.row, col: curNode.own.col })
                        if curNode.isLeaf()
                            @model.makeMove(@_curMoveSeq)
                        else
                            @_curTreeTrace.push(curNode.next)
                            @$el.find("#cell-#{row}-#{col}").addClass("selected-cell")
                            @$el.find(".cell").removeClass("option-cell")
                            for sub in curNode.next
                                @$el.find("#cell-#{sub.own.row}-#{sub.own.col}").addClass("option-cell")


            #--- STATE INTERACTIONS --------------------------------------------------

            # Reset the view state.
            _updateView: =>
                @_updateBoardState()
                @_resetCurMoveBuffer()

            # Update the displayed board state to match the board state in the model.
            _updateBoardState: =>
                @$el.find(".cell").removeClass("checker-white checker-black queen-white queen-black")

                for row in [1..@model.get("boardSize")]
                    for col in [1..@model.get("boardSize")]
                        cellContents = @model.getCellContents(row, col)
                        curCell = $("#cell-#{row}-#{col}")

                        if cellContents is constants.pieces.BLACK_CHECKER
                            curCell.addClass("checker-black")
                        else if cellContents is constants.pieces.WHITE_CHECKER
                            curCell.addClass("checker-white")
                        else if cellContents is constants.pieces.BLACK_QUEEN
                            curCell.addClass("queen-black")
                        else if cellContents is constants.pieces.WHITE_QUEEN
                            curCell.addClass("queen-white")

            # Reset current move buffers.
            _resetCurMoveBuffer: ->
                @_curMoveSeq = []
                @_curTreeTrace = [ @model.availableMoves ]

                @$el.find(".cell").removeClass("option-cell selected-cell")
                for sub in @model.availableMoves
                    @$el.find("#cell-#{sub.own.row}-#{sub.own.col}").addClass("option-cell")


            #--- UTILITY -------------------------------------------------------------

            # Get the coordinates of the given cell element.
            _getCellCoords: (cell) ->
                [row, col] = $(cell).attr("id").split("-")[1..2]
                [parseInt(row, 10), parseInt(col, 10)]


        GameView
)
